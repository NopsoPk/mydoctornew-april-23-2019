import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Customer } from '../../providers/SalonAppUser-Interface';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { ToastController } from 'ionic-angular';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { Keyboard } from '@ionic-native/keyboard';
import { LoadingController, Platform } from 'ionic-angular';
import { GlobalProvider } from './../../providers/global/global';
import { SalonServicesModal } from '../../providers/salon-services-modal/salon-services-modal';
import { MenuController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { MainHomePage } from '../main-home/main-home';
import { HomePage } from '../home/home';
import { AUTH_CODE_VALIDATION } from '../../providers/SalonAppUser-Interface';
import { DatePipe } from '@angular/common'
import { config } from '../../providers/config/config';
@IonicPage()
@Component({
  selector: 'page-login-sign-up',
  templateUrl: 'login-sign-up.html',
})
export class LoginSignUpPage {
  /* customer signUpSection */
  public unregisterBackButtonAction: any;
  customerProfileExist = false
  showCustomerProfile = false
  firstname = '';
  email = '';
  PhoneNumber: '';
  responseJson: any[];
  response: string;
  location = '';
  deviceType: string;
  errorMessageName = '';
  errorMessagePhone = '';
  errorMessageEmail = '';
  errorMessageLocation = '';
  isValidName = true;
  isValidPhone = true;
  isValidEmail = true;
  isValidLocation = true;
  isValidLatLan = false;
  isGenderMale = true;
  isGenderSelected = false;
  customerGender: string
  postCode: string;
  longitude: any;
  latitude: any;
  authCodeRegister: any;
  cust_phone_sign_up: any = '';
  emailExpression = '^[\w\.]+@([\w]+\.)+[A-Z]{2,7}$';
  paramRegister = null
  /* end customer signUp */
  /* customer Login */
  authCodeLogin: any;
  isValidPhoneLogin = true;
  errorMessageLogin: any = ''
  cust_phone_login: any = ''
  /* end customer Login */
  shouldLogin: any
  shouldRegister: any = true
  shouldOtp: any
  shouldOtpLogin: any
  previousPage: any
  public headerLabelMessage = ''
  public objAuthCodeValidtion: AUTH_CODE_VALIDATION
  status: string;
  blockTime = '';
  blockTimeRemeaning = 0;
  blockAttempts_code = 0;
  isBlock_code = false;
  blockTime_code = '';
  blockTimeRemeaning_code = 0;
  blockAttempts = 0;
  isBlock = false;
  public myAppName = ''
  /* zahid added these vairables  */
  public selectedScreen: number
  public beforeGoingToOTP: number
  public loginSignUpScreens = { "shouldPopScreen": 0, "LoginScreenSelected": 1, "SignUpScreenSelected": 2, "OTPScreenSelected": 3 }
  constructor(
    public datePipe: DatePipe,
    public geolocation: Geolocation,
    public keyBoard: Keyboard,
    public navCtrl: NavController,
    public storage: Storage,
    public platform: Platform,
    public loader: LoadingController,
    public serviceManager: GlobalProvider,
    public salonServicesModal: SalonServicesModal,
    private toastCtrl: ToastController,
    private salonServiceModal: SalonServicesModal,
    private loadingController: LoadingController,
    private menu: MenuController,
    private navParams: NavParams,
    private viewCtrl: ViewController,
    public customerModal: CustomerModal,
    private nativePageTransitions: NativePageTransitions,
    public events: Events,
  ) {
    Object.freeze(this.loginSignUpScreens)
    this.shouldRegister = true;
    this.selectedScreen = this.loginSignUpScreens.SignUpScreenSelected
    this.myAppName = config.AppName
    this.isGenderMale = false;
    this.customerGender = 'female'
    this.isGenderSelected = true
    this.previousPage = this.navParams.get('comingFrom')
    this.previousPage === 'SalonDetailsPage' ? this.headerLabelMessage = 'To make an appointment please provide details below: ' : this.headerLabelMessage = 'To make an appointment please provide details below: '
    if (this.platform.is('ios')) {
      this.deviceType = "1";
    }
    if (this.platform.is('android')) {
      this.deviceType = "2";
    }
    this.updatelocation();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginSignUpPage');
  }
  /* zahid added new back button  */
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    if (this.selectedScreen === this.loginSignUpScreens.SignUpScreenSelected) {
      if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(MainHomePage)
      }
    } else {
      switch (this.selectedScreen) {
        case this.loginSignUpScreens.LoginScreenSelected:
          this.nativePageTransitions.slide(options)
          this.switchToRegister()
          break;
        case this.loginSignUpScreens.SignUpScreenSelected:
          this.nativePageTransitions.slide(options)
          this.switchToRegister()
          break;
        case this.loginSignUpScreens.OTPScreenSelected:
          this.nativePageTransitions.slide(options)
          this.beforeGoingToOTP === this.loginSignUpScreens.SignUpScreenSelected ? this.switchToRegister() : this.switchToLogin()
          break;
        default:
          break;
      }
    }
  }
  /* signUp Section */
  isValidLastIndex(value) {
    if (value == '0' || value == '1' || value == '2' || value == '3' || value == '4' || value == '5' || value == '6' || value == '7' || value == '8' || value == '9') {
      return true
    } else {
      return false;
    }
  }
  OnValueEnterPhone(value) {
    var lastChar = value[value.length - 1];
    let elementChecker: string;
    elementChecker = value;
    if (this.isValidLastIndex(lastChar)) {
      this.isValidPhone = true;
      if (this.cust_phone_sign_up.length == 1) {
        if (this.cust_phone_sign_up != '0') {
          this.cust_phone_sign_up = '';
          this.isValidPhone = false;
          this.errorMessagePhone = AppMessages.msgMobileNumberStartingDigits
        }
      } else if (this.cust_phone_sign_up.length == 2) {
        if (this.cust_phone_sign_up != '03') {
          this.cust_phone_sign_up = '';
          this.isValidPhone = false;
          this.errorMessagePhone = AppMessages.msgMobileNumberStartingDigits
        }
      } else if (this.cust_phone_sign_up.length == 0) {
        this.errorMessagePhone = AppMessages.msgMobileNumberStartingDigits
      }
    } else {
      this.cust_phone_sign_up = elementChecker.slice(0, -1);
    }
  }
  isValidPhoneInput() {
    if (this.cust_phone_sign_up.length == 0) {
      this.isValidPhone = false;
      this.errorMessagePhone = AppMessages.msgMobileNumberStartingDigits
    } else if (this.cust_phone_sign_up.length == 11) {
      this.isValidPhone = true;
      this.errorMessagePhone = ''
    }
  }
  OnValueEnterName(name) {
    this.isValidNameInput();
  }
  OnValueEnterEmail(email) {
    // this.isValidEmailInput();
  }
  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'top',
      cssClass: "ToastClass",
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }
  OnValueEnterLocation(location) {
    this.isValidLocationInput();
  }
  OnCompleteSignUp() {
    this.checkValidation();
    if (this.isValidEmail && this.isValidName && this.isValidPhone && this.isGenderSelected) {
      this.getOtpFromServer();
    } else {
      console.log("Activation can't completes with errors");
    }
  }
  ionViewWillLeave() {
    /** Unregister the custom back button action for this page */
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(MainHomePage)
      }
    }
  }
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }
  getOtpFromServer() {
    let notificationController = this.loadingController.create({
      content: "Please wait.."
    });
    notificationController.present();
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      service: btoa('send_sms'),
      phone_number: btoa(this.cust_phone_sign_up),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((res) => {
        if (Number(res.status) === 1) {
          let token = this.serviceManager.getFromLocalStorage(this.serviceManager.GCM_TOKEN)
          let cust_Gender = ''
          if (this.isGenderMale) {
            cust_Gender = this.serviceManager.CUSTOMER_GENDER_MALE
          } else {
            cust_Gender = this.serviceManager.CUSTOMER_GENDER_FEMALE
          }
          this.paramRegister = {
            service: btoa('auth_and_register'),
            cust_name: btoa(this.firstname),
            cust_lat: btoa(this.latitude),
            auth_code: btoa(this.authCodeRegister),
            cust_lng: btoa(this.longitude),
            cust_phone: btoa(this.cust_phone_sign_up),
            cust_email: btoa(this.email),
            cust_gender: btoa(cust_Gender),
            cust_device_id: btoa(token),
            device_datetime: btoa(this.serviceManager.getCurrentDateTime()),
            cust_device_type: btoa(this.deviceType),
          }
          this.switchToOtpSignUp();
        } else {
          this.serviceManager.makeToastOnFailure(res.message)
        }
        console.log('OtpResponse', JSON.stringify(res))
        notificationController.dismiss();
      },
        error => {
          notificationController.dismiss();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }
      );
  }
  updatelocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = resp.coords.latitude.toString()
      this.longitude = resp.coords.longitude.toString()
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }
  OnValueEnterOtp(otp) {
    if (this.authCodeRegister && this.authCodeRegister.length >= 4) {
      this.authCodeRegister = otp
      this.paramRegister.auth_code = btoa(this.authCodeRegister)
      this.resgisterUser()
      console.log('ParamsRegister', JSON.stringify(this.paramRegister))
    }
  }
  checkValidation() {
    this.isValidNameInput();
    if (this.email.trim().length > 0) this.checkEmailExpression();
    else this.isValidEmail = true
    this.isValidPhoneInput();
  }
  isValidNameInput() {
    if (this.firstname.length == 0) {
      this.isValidName = false;
      this.errorMessageName = 'Please provide your name'
    } else {
      this.isValidName = true;
      this.errorMessageName = ''
    }
  }
  isValidEmailInput() {
    if (this.email.length == 0) {
      this.isValidEmail = false;
      this.errorMessageEmail = 'Please provide your email'
    } else {
      this.isValidEmail = true;
      this.errorMessageEmail = ''
      /*check email pattern */
    }
  }
  checkEmailExpression() {
    if (this.email.length > 0) {
      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.email)) {
        this.isValidEmail = true;
        this.errorMessageEmail = ''
      } else {
        this.isValidEmail = false;
        this.errorMessageEmail = 'Please provide your valid email address'
      }
    }
  }
  checkLocationFormat() {
    if ((this.location.length < 6)) {
      this.isValidLocation = false;
      this.errorMessageLocation = 'Please enter a valid postcode including a space.'
    } else if (!this.checkSpaces()) {
      this.isValidLocation = false;
      this.errorMessageLocation = 'Invalid postcode'
    }
  }
  checkSpaces() {
    let spaceFound = false;
    let spaceCOunter = 0;
    let text = this.location;
    for (let i = 0; i < text.length; i++) {
      let valueATIndex = text.charAt(i);
      let value = valueATIndex.toString();
      console.log('value', value);
      if (value == ' ') {
        console.log('spaceFound', value);
        spaceFound = true;
      } else {
        spaceFound = false;
      }
      if (spaceFound) {
        spaceCOunter = (spaceCOunter) + 1;
      }
    }
    console.log('spaceCOunter', spaceCOunter);
    if (spaceCOunter < 1 || spaceCOunter > 1) {
      return false;
    }
    return true;
  }
  isValidLocationInput() {
    if (this.location.length == 0) {
      this.isValidLocation = false;
      this.errorMessageLocation = 'Please provide your post code'
    } else {
      this.isValidLocation = true;
      this.errorMessageLocation = ''
    }
  }
  resgisterUser() {
    let notificationController = this.loadingController.create({
      content: "Please wait..."
    });
    notificationController.present();
    this.serviceManager.getData(this.paramRegister)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((response) => {
        this.response = response.status;
        this.responseJson = response;
        notificationController.dismiss();
        console.log('ResponseRegister', JSON.stringify(response));
        if (response.status === '1') {
          this.storage.set('isLogin', true);
          this.saveCustomerIntoDB(response.customer)
        } else {
          this.clearInputOtpRegister();
          this.objAuthCodeValidtion = this.serviceManager.getFromLocalStorage('AUTH_CODE_VALIDATION')
          if (this.objAuthCodeValidtion) {
            this.objAuthCodeValidtion.wrongAuthCodeEnteredCount += 1
            if (this.objAuthCodeValidtion.wrongAuthCodeEnteredCount >= 5) {
              this.objAuthCodeValidtion.isUserBlocked = true;
              this.objAuthCodeValidtion.serverTimeWhenUserWasBlocked = response.response_datetime;
              this.objAuthCodeValidtion.currentServerTime = response.response_datetime;
            }
            this.serviceManager.setInLocalStorage('AUTH_CODE_VALIDATION', this.objAuthCodeValidtion)
          } else {
            /** for the first time user has entered wrong activation code. */
            this.objAuthCodeValidtion = {
            }
            this.objAuthCodeValidtion.serverTimeWhenUserWasBlocked = response.response_datetime;
            this.objAuthCodeValidtion.currentServerTime = response.response_datetime;
            this.objAuthCodeValidtion.wrongAuthCodeEnteredCount = 1
            this.serviceManager.setInLocalStorage('AUTH_CODE_VALIDATION', this.objAuthCodeValidtion)
          }
          this.serviceManager.makeToastOnFailure(response.error)
        }
        if (response.favorites) {
        }
      },
        error => {
          notificationController.dismiss();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }
      );
  }
  genderMaleTapped() {
    this.isGenderSelected = true
    this.isGenderMale = true
  }
  genderFemaleTapped() {
    this.isGenderSelected = true
    this.isGenderMale = false
  }
  saveCustomerIntoDB(customer: Customer) {
    if (customer) {
      this.customerModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.customerModal.createCustomerTable().then(isTableCreated => {
            if (isTableCreated === true) {
              this.customerModal.InsertInToCustomerTable(customer).then(isCustomerInserted => {
                if (isCustomerInserted === true) {
                  console.log('success: InsertInToCustomerTable ');
                  this.serviceManager.setInLocalStorage(this.serviceManager.IS_USER_LOGIN, true)
                  this.storage.set('isLogin', true);
                  this.events.publish('customer:changed', customer);
                  // if (this.previousPage == "SalonDetailsPage" || this.previousPage == "AppointmentsPage") {
                  if (this.navCtrl.canGoBack()) {
                    this.navCtrl.pop();
                  } else {
                    this.navCtrl.setRoot(MainHomePage)
                  }
                  // } else {
                  //   this.events.publish('customer:changed', 'customer has changed');
                  //   this.events.subscribe('getCustomer', () => {
                  //     this.events.publish('sendCustomer', 'customer has changed');
                  //   })
                  //   this.navCtrl.setRoot(MainHomePage)
                  // }
                } else {
                  console.log('error: InsertInToCustomerTable ', isCustomerInserted);
                }
              }, error => {
                console.log('error: ', error.message);
              })
            }
          })
        }
      })
    }
  }
  /** 
   * end SignUpSection 
   * switch user to login or registration on user selection 
   * end login section
  */
  isValidPhoneInputLogin() {
    if (this.cust_phone_login.length == 0) {
      this.isValidPhoneLogin = false;
      this.errorMessageLogin = AppMessages.msgMobileNumberStartingDigits
    } else if (this.cust_phone_login.length == 11) {
      this.isValidPhoneLogin = true;
      this.errorMessageLogin = ''
    } else {
      this.isValidPhoneLogin = false;
      this.errorMessageLogin = AppMessages.msgMobileNumberStartingDigits
    }
  }
  switchToLogin() {
    this.shouldOtpLogin = false
    this.shouldLogin = true
    this.shouldRegister = false
    this.shouldOtp = false
    this.selectedScreen = this.loginSignUpScreens.LoginScreenSelected
  }
  switchToRegister() {
    this.shouldOtpLogin = false
    this.shouldRegister = true;
    this.shouldLogin = false
    this.shouldOtp = false
    this.selectedScreen = this.loginSignUpScreens.SignUpScreenSelected
  }
  switchToOtpSignUp() {
    this.shouldOtpLogin = false
    this.shouldOtp = true
    this.shouldRegister = false;
    this.shouldLogin = false
    this.beforeGoingToOTP = this.loginSignUpScreens.SignUpScreenSelected
    this.selectedScreen = this.loginSignUpScreens.OTPScreenSelected
  }
  switchToOtpLogin() {
    this.shouldOtp = false
    this.shouldOtpLogin = true
    this.shouldRegister = false;
    this.shouldLogin = false
    this.beforeGoingToOTP = this.loginSignUpScreens.LoginScreenSelected
    this.selectedScreen = this.loginSignUpScreens.OTPScreenSelected
  }
  /*otp Login*/
  OnValueEnterPhoneLogin(value) {
    var lastChar = value[value.length - 1];
    let elementChecker: string;
    elementChecker = value;
    if (this.isValidLastIndex(lastChar)) {
      this.isValidPhoneLogin = true;
      if (this.cust_phone_login.length == 1) {
        if (this.cust_phone_login != '0') {
          this.cust_phone_login = '';
          this.isValidPhoneLogin = false;
          this.errorMessageLogin = AppMessages.msgMobileNumberStartingDigits
        }
      } else if (this.cust_phone_login.length == 2) {
        if (this.cust_phone_login != '03') {
          this.cust_phone_login = '';
          this.isValidPhoneLogin = false;
          this.errorMessageLogin = AppMessages.msgMobileNumberStartingDigits
        }
      } else if (this.cust_phone_login.length == 0) {
        this.errorMessageLogin = AppMessages.msgMobileNumberStartingDigits
      }
    } else {
      this.cust_phone_login = elementChecker.slice(0, -1);
    }
  }
  OnLogin() {
    this.isValidPhoneInputLogin();
    if (this.isValidPhoneLogin) {
      this.getOtpFromServerLogin();
    } else {
      console.log("Activation can't completes with errors");
    }
  }
  /**customer login section */
  getOtpFromServerLogin() {
    let notificationController = this.loadingController.create({
      content: "Please wait.."
    });
    notificationController.present();
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      service: btoa('send_sms'),
      cust_type: btoa('existing'),
      phone_number: btoa(this.cust_phone_login),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((res) => {
        if (res.cust_id && res.cust_id != undefined) {
          console.log('res', JSON.stringify(res))
          if (Number(res.status) === 1) {
            this.serviceManager.makeToastOnSuccess(res.message)
            this.switchToOtpLogin();
          } else {
            this.serviceManager.makeToastOnFailure(res.message)
          }
          console.log('OtpResponse', JSON.stringify(res))
        } else {
          this.switchToRegister()
          this.serviceManager.makeToastOnFailure(res.error)
        }
        notificationController.dismiss();
      },
        error => {
          notificationController.dismiss();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }
      );
  }
  OnValueEnterOtpLogin(otp) {
    if (this.authCodeLogin && this.authCodeLogin.length >= 4) {
      this.authCodeLogin = otp
      this.validateAuthCodeFromServer()
    }
  }
  validateAuthCodeFromServer() {
    let notificationController = this.loadingController.create({
      content: "Authenticating..."
    });
    notificationController.present();
    let token = this.serviceManager.getFromLocalStorage(this.serviceManager.GCM_TOKEN);
    var params = {
      service: btoa('validate_auth_code'),
      phone_number: btoa(this.cust_phone_login),
      cust_device_id: btoa(token),
      device_datetime: btoa(this.serviceManager.getCurrentDateTime()),
      auth_code: btoa(this.authCodeLogin),
      cust_device_type: btoa(this.deviceType),
      cust_lat: btoa(this.latitude),
      cust_lng: btoa(this.longitude)
    }
    console.log('ParamsValidateAuthCode', JSON.stringify(params))
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((response) => {
        notificationController.dismiss();
        if (Number(response.status) === 1) {
          if (response.favorites) {
          }
          let customer = response.customer
          if (customer.cust_lat.trim().length === 0 || customer.cust_lng.trim().length === 0 && this.latitude) {
            customer.cust_lat = this.latitude
            customer.cust_lng = this.longitude
          }
          let promo_code = response.promo_code;
          if (promo_code && promo_code.pc_id != undefined) {
            this.saveCustomerLogin(customer, response.promo_code)
          } else {
            console.log('else')
            promo_code = null
            this.saveCustomerLogin(customer, null)
          }
        }
        else {
          this.clearInputOtpLogin();
          this.objAuthCodeValidtion = this.serviceManager.getFromLocalStorage('AUTH_CODE_VALIDATION')
          if (this.objAuthCodeValidtion) {
            this.objAuthCodeValidtion.wrongAuthCodeEnteredCount += 1
            if (this.objAuthCodeValidtion.wrongAuthCodeEnteredCount >= 5) {
              this.objAuthCodeValidtion.isUserBlocked = true;
              this.objAuthCodeValidtion.serverTimeWhenUserWasBlocked = response.response_datetime;
              this.objAuthCodeValidtion.currentServerTime = response.response_datetime;
            }
            this.serviceManager.setInLocalStorage('AUTH_CODE_VALIDATION', this.objAuthCodeValidtion)
          } else {
            this.objAuthCodeValidtion = {
            }
            this.objAuthCodeValidtion.serverTimeWhenUserWasBlocked = response.response_datetime;
            this.objAuthCodeValidtion.currentServerTime = response.response_datetime;
            this.objAuthCodeValidtion.wrongAuthCodeEnteredCount = 1
            this.serviceManager.setInLocalStorage('AUTH_CODE_VALIDATION', this.objAuthCodeValidtion)
          }
          this.serviceManager.makeToastOnFailure(response.error)
        }
      },
        error => {
          notificationController.dismiss();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }
      );
  }
  clearInputOtpLogin() {
    this.authCodeLogin = '';
  }
  clearInputOtpRegister() {
    this.authCodeRegister = '';
  }
  saveCustomerLogin(customer: Customer, promo_code) {
    if (customer) {
      this.customerModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.customerModal.createCustomerTable().then(isTableCreated => {
            if (isTableCreated === true) {
              this.customerModal.InsertInToCustomerTable(customer).then(isCustomerInserted => {
                if (isCustomerInserted === true) {
                  console.log('success: InsertInToCustomerTable ');
                  this.serviceManager.setInLocalStorage(this.serviceManager.IS_USER_LOGIN, true)
                  this.storage.set('isLogin', true);
                  this.events.publish('customer:changed', customer);
                  // if (this.previousPage == "SalonDetailsPage" || this.previousPage == "AppointmentsPage") {
                  if (this.navCtrl.canGoBack()) {
                    this.navCtrl.pop();
                  } else {
                    this.navCtrl.setRoot(MainHomePage)
                  }
                  // } else {
                  //   this.events.publish('customer:changed', 'customer has changed');
                  //   this.events.subscribe('getCustomer', () => {
                  //     this.events.publish('sendCustomer', 'customer has changed');
                  //   })
                  //   this.btnBackTapped()
                  // }
                  if (promo_code && promo_code.pc_id != undefined) {
                  } else {
                  }
                } else {
                  console.log('error: InsertInToCustomerTable ', isCustomerInserted);
                }
              }, error => {
                console.log('error: ', error.message);
              })
            }
          })
        }
      })
    }
  }
  resendActivationCode(cust_phone) {
    this.getBlockTime();
    if (!this.isBlock) {
      this.resendCode(cust_phone);
    } else {
      if (this.blockTimeRemeaning >= 60) {
        this.resetBlock();
        this.resendCode(cust_phone);
      } else {
        this.serviceManager.makeToastOnFailure(AppMessages.msgFourTimesAuthentication);
        console.log('IF', 'in if' + this.blockTime + 'hello');
      }
    }
  }
  resetBlock() {
    this.storage.set('blockTime', '');
    this.storage.set('isBlock', false);
    this.storage.set('blockAttempts', '');
  }
  resetBlock_code() {
    this.storage.set('blockTime_code', '');
    this.storage.set('isBlock_code', false);
    this.storage.set('blockAttempts_code', '');
  }
  resendCode(phone_number) {
    let notificationController = this.loadingController.create({
      content: "Resending the activation code."
    });
    notificationController.present();
    var params = {
      service: btoa('send_sms'),
      phone_number: btoa(phone_number),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((response) => {
        this.status = response.status;
        notificationController.dismiss();
        console.log('AllResponse', response);
        this.serviceManager.makeToastOnSuccess('Activation code is sent to your number');
        if (!this.isBlock) {
          this.setAttempts();
        }
        this.getBlockedAttempts();
      },
        error => {
          notificationController.dismiss();
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);
        }
      );
  }
  setAttempts() {
    this.storage.get('blockAttempts').then((value) => {
      if (value != undefined && value != null) {
        value = (value + 1);
      } else {
        value = 1;
      }
      if (value >= 4) {
        let blockTime1 = this.serviceManager.getCurrentDateTime();
        this.storage.set('blockTime', blockTime1);
        this.storage.set('isBlock', true);
      } else {
        this.storage.set('isBlock', false);
      }
      this.storage.set('blockAttempts', value);
    });
  }
  getBlockedAttempts() {
    this.storage.get('blockAttempts').then((value) => {
      this.blockAttempts = value;
    });
    this.storage.get('blockTime').then((value) => {
      this.blockTime = value;
    });
    this.storage.get('isBlock').then((value) => {
      this.isBlock = value;
    });
  }
  getBlockTime() {
    let date = new Date();
    let currentDateTimeInString = this.datePipe.transform(date, 'yyyy-MM-dd HH:mm:ss');
    return this.blockTimeRemeaning = Math.floor((new Date(currentDateTimeInString).getTime() - new Date(this.blockTime).getTime()) / 60000);
  }
}