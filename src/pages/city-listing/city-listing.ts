import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite } from '@ionic-native/sqlite';
import { HomePage } from '../home/home';
import { HomeModal } from "./../../providers/homeModal/homeModal";

import { GlobalProvider } from "../../providers/global/global";
import {
  NativePageTransitions,

  NativeTransitionOptions
} from "@ionic-native/native-page-transitions";
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db'
import {
  CityInfo
} from "./../../providers/SalonAppUser-Interface";

import {
  Platform,
  MenuController,
} from "ionic-angular";
/**
 * Generated class for the CityListingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-city-listing',
  templateUrl: 'city-listing.html',
})
export class CityListingPage {
  public IsFirstRequest = true
  public txtSearchSalon = "";
  public unregisterBackButtonAction: any;
  options: NativeTransitionOptions = {
    direction: "left",
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation
    fixedPixelsBottom: 0 // not to include this Bottom section in animation
  };

  public CityInfo: CityInfo[] = [];
  constructor(public navCtrl: NavController,
    public sqlProvider: SqliteDbProvider,
    public serviceManager: GlobalProvider,
    public platform: Platform,
    private menu: MenuController,
    private sqlite: SQLite,
    public homeModal: HomeModal,
    private nativePageTransitions: NativePageTransitions,
    public navParams: NavParams) {
  }
  ionViewDidLoad() {
    this.getCityListing();
  }
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();

  }
  getCityListing() {
    // this.sqlProvider.getDatabaseState().subscribe(ready => {
    //   if (ready) {
    //     this.sqlProvider.getAllCityNames().then(strSalonIDs => {
    //       let city_names = strSalonIDs
    //       let SplitCityNames = city_names.split(",");
    //       SplitCityNames.forEach(element => {
    //         let cityName: string = element;
    //         let cityInfoObj = {
    //           city_name: cityName,
    //           city_abr: 'City Name'
    //         }
    //         this.CityInfo.push(cityInfoObj)
    //       });


    //     })
    //   }
    // })

    let city_names = this.serviceManager.getFromLocalStorage(
      this.serviceManager.city_name
    );
    if(city_names){
      this.CityInfo=[]
      this.CityInfo =city_names.split(",");
    }
    if (this.IsFirstRequest) {
      this.IsFirstRequest = false
      this.getCitiesFromServer();
    }
  }
  getCitiesFromServer() {
    // let loadingController = this.loadingController.create({
    //   content: "Fetching Home page"
    // });
    // loadingController.present();
    if(!this.CityInfo){
      this.serviceManager.showProgress('Fetching doctor specialities  Please Wait...')
    }
    
    const params = {
      service: btoa("get_salon_cities"),
    };
    console.log("params" + JSON.stringify(params));
    this.serviceManager.getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })
      .subscribe(
        res => {
          if(!this.CityInfo){
            this.serviceManager.stopProgress()
          }
          this.CityInfo=[]
          let cities = ''
          res.sal_cities.forEach(city => {
            cities += city.sal_city + ','
            this.CityInfo.push(city.sal_city)
          });

          cities = cities.slice(0, cities.length - 1)
          this.serviceManager.setInLocalStorage(this.serviceManager.city_name, cities);
        },
        error => {
          if(!this.CityInfo){
            this.serviceManager.stopProgress()
          }
        }
      );
  }
  btnBackTapped() {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(HomePage);
    }
  }

  private customHandleBackButton(): void {
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(HomePage);
      }
    }

    // this.nativePageTransitions.slide(this.options)
    // if (this.globalSearch.search_keyword.length != 0) {
    // this.navCtrl.push(LookingForPage, { isHideTab: "1" }, { animate: false })
    // } else {
    //   this.navCtrl.push(AppointmentsCardPage, { isHideTab: "1" }, { animate: false })
    // }
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      if (this.menu.isOpen()) {
        this.menu.close();
      } else {
        this.customHandleBackButton();
        // if (this.alertShown == false) {
        //   this.alertShown = true
        //   this.customHandleBackButton();
        //   this.presentConfirm();
        // } else {
        //   this.alertShown = false
        //   this.alert.dismiss()
        // }
      }
    }, 10);
  }

  openDetailsPage(cityName, event) {
    this.nativePageTransitions.slide(this.options);
    this.navCtrl.push(HomePage, {
      cityname: cityName,
      isFromCityListing: true,
    });
  }

}