import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CityListingPage } from './city-listing';

@NgModule({
  declarations: [
    CityListingPage,
  ],
  imports: [
    IonicPageModule.forChild(CityListingPage),
  ],
})
export class CityListingPageModule {}
