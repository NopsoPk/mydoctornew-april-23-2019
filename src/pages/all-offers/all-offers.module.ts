import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllOffersPage } from './all-offers';

@NgModule({
  declarations: [
    AllOffersPage,
  ],
  imports: [
    IonicPageModule.forChild(AllOffersPage),
  ],
})
export class AllOffersPageModule {}
