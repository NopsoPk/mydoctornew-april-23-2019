import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BeautyTipsDetailPage } from './beauty-tips-detail';

@NgModule({
  declarations: [
    BeautyTipsDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(BeautyTipsDetailPage),
  ],
})
export class BeautyTipsDetailPageModule {}
