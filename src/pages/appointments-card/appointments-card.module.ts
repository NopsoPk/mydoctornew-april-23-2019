import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentsCardPage } from './appointments-card';

@NgModule({
  declarations: [
    AppointmentsCardPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentsCardPage),
  ],
})
export class AppointmentsCardPageModule {}
