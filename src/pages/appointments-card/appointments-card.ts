import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Platform,
  LoadingController
} from "ionic-angular";
import { MenuController } from "ionic-angular";
import { HomeModal } from "./../../providers/homeModal/homeModal";
import { GlobalProvider } from "./../../providers/global/global";
import { Homepage, specialties } from "../../providers/SalonAppUser-Interface";
import { config } from "./../../providers/config/config";
import { NOPSO_Header_OPTIONS } from "../../providers/SalonAppUser-Interface";
import { GloballySharedData } from "../../providers/global-service/global-service";
import { TabsPage } from "../tabs/tabs";
// import { FavoriteSalonsPage } from '../favorite-salons/favorite-salons'
// import { FavoriteModal } from '../../providers/FavoriteModal/FavoriteModal';
import {
  NativePageTransitions,
  NativeTransitionOptions
} from "@ionic-native/native-page-transitions";
import { MainHomePage } from "../main-home/main-home";
import { HomePage } from "../home/home";
import { SalonServicesModal } from "./../../providers/salon-services-modal/salon-services-modal";
import { SqliteDbProvider } from "../../providers/sqlite-db/sqlite-db";
import { CustomerModal } from "../../providers/CustomerModal/CustomerModal";
import { Customer } from "../../providers/SalonAppUser-Interface";
@IonicPage()
@Component({
  selector: "page-appointments-card",
  templateUrl: "appointments-card.html"
})
export class AppointmentsCardPage {
  public unregisterBackButtonAction: any;
  public myCustomer: Customer;
  nextPage: any;
  public IsFirstRequest = true
  tabBarElement: any;
  public isFavourites = false;
  public nopsoHeaderOptions: NOPSO_Header_OPTIONS;
  sectionParameters: any;
  // homepageData: Homepage[] = [];
  specialtieList: specialties[] = [];
  options: NativeTransitionOptions = {
    direction: "left",
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation
    fixedPixelsBottom: 0 // not to include this Bottom section in animation
  };

  public homeImageUrl = config.salonImgUrl; 
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: GlobalProvider,
    private loadingController: LoadingController,
    public salonServicesModal: SalonServicesModal,
    public platform: Platform,
    public salonModal: SqliteDbProvider,
    // public favModel: FavoriteModal,
    public globalSearch: GloballySharedData,
    public globalService: GloballySharedData,
    private nativePageTransitions: NativePageTransitions,
    public customerModal: CustomerModal,
    private menu: MenuController,
    public homeModal: HomeModal
  ) { }
  ionViewWillEnter() {
    this.getCustomer();
  }

  getCustomer() {
    if (this.myCustomer) {
      return;
    }
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(
          customer => {
            if (customer) {
              this.myCustomer = customer;
            }
          },
          error => { }
        );
      }
    });
  }

  getHomePageSection(progress) {

    // let loadingController = this.loadingController.create({
    //   content: "Fetching Home page"
    // });
    // loadingController.present();
    if(progress){
      this.serviceManager.showProgress('Fetching doctor specialities  Please Wait...')
    }
    const params = {
      service: btoa("local_data_single"),
      lds_id: btoa("7")
    };
    console.log("params" + JSON.stringify(params));
    this.serviceManager.getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })
      .subscribe(
        res => {
          if(progress){
            this.serviceManager.stopProgress()
          }
          // this.serviceManager.stopProgress()
          // loadingController.dismissAll();
          if (res.data) {
            this.SaveHomePageDataIntoDB(res.data.specialties);
            console.log("responseFav" + JSON.stringify(res));
          }
        },
        error => {
          if(progress){
            this.serviceManager.stopProgress()
          }
          // loadingController.dismissAll();
        }
      );
  }

  SaveHomePageDataIntoDB(homepageDataToSave) {
    if (!homepageDataToSave || homepageDataToSave.length === 0) {
      return;
    }
    this.homeModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.homeModal.createSpecialityTable().then(
          istblHomePageSectionsCreated => {
            if (istblHomePageSectionsCreated) {
              this.homeModal.InsertInToSpecilityTable(homepageDataToSave).then(
                res => {
                  this.getHomePageSectionsFromDB();
                },
                error => { }
              );
            }
          },
          error => { }
        );
      } else {
      }
    });
  }

  getFavSalonFromServer() {
    const params = {
      service: btoa("get_favorites"),
      cft_id: btoa("1"),
      cust_id: btoa(this.myCustomer.cust_id)
    };
    console.log("params" + JSON.stringify(params));
    this.serviceManager
      .getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })
      .subscribe(
        res => {
          console.log("responseFav" + JSON.stringify(res));
          if (res.favorites) {
          }
        },
        error => { }
      );
  }

  sideMenuAndTabBarEvents() {
    let HideTabe = this.navParams.get("isHideTab");
    if (HideTabe !== null && HideTabe == "1") {
      for (let i = 0; i < this.navCtrl.length() - 1; i++) {
        this.navCtrl.remove(this.navCtrl.getPrevious().index);
      }

      this.tabBarElement = document.querySelector(".tabbar.show-tabbar");
      if (this.tabBarElement) {
        this.tabBarElement.style.display = "none";
      }
    }
  }

  ionViewDidLoad() {
    this.getHomePageSectionsFromDB();
    console.log("ionViewDidLoad AppointmentsCardPage");
  }
  getHomePageSectionsFromDB() {
    this.homeModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.homeModal.getSpeciality().then(
          specialtie => {
            // let data: specialties[] = specialtie
            // let abc =   data.sort( this.compare );
            
            this.specialtieList = specialtie;
            this.specialtieList.forEach(el => {
              // alert(el.sp_name)
            });
            if (this.specialtieList && this.specialtieList.length > 0) {

              if (this.IsFirstRequest) {
                this.IsFirstRequest = false
                this.getHomePageSection(false);
              }
            } else {
              if (this.IsFirstRequest) {
                this.IsFirstRequest = false
                this.getHomePageSection(true);
              }

            }
          },
          error => { }
        );
      }
    });
  }

  //custom back button
   compare( a, b ) {
    if ( a.sp_name < b.sp_name ){
      return -1;
    }
    if ( a.sp_name > b.sp_name ){
      return 1;
    }
    return 0;
  }
  
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      () => {
        this.customHandleBackButton();
      },
      10
    );
  }
  private customHandleBackButton(): void {
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(HomePage);
      }
    }
  }
  //end custom back button
  btnBackTapped() {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(HomePage);
    }
  }

  findNextPage(homePageSection: specialties) {
    // this.globalService.sty_id = homePageSection.sectionParameters; //to get salon for salon type id in salon listing page......
    this.serviceManager.setInLocalStorage("homePageSection", homePageSection);
    this.nextPage = TabsPage;
    // this.sectionParameters = homePageSection.sectionParameters;
    this.navCtrl.push(HomePage, {
      styId: this.globalService.sty_id,
      sp_name: homePageSection.sp_name
    });
  }
}
