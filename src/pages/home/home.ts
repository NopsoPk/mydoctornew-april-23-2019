import { CustomerModal } from "./../../providers/CustomerModal/CustomerModal";
import { SalonServicesModal } from "./../../providers/salon-services-modal/salon-services-modal";
import { SalonServices, APP_CONFIG } from "./../../providers/SalonAppUser-Interface";
import { SQLiteObject, SQLite } from "@ionic-native/sqlite";
import { Keyboard } from "@ionic-native/keyboard";
import { Geolocation } from '@ionic-native/geolocation';
import { Sal_Techs, Tech_appointments } from './../../providers/SalonAppUser-Interface';
import { TECH_HOURS } from '../../providers/SalonAppUser-Interface';
import { TechnicianModal } from '../../providers/TechnicianModal/TechnicianModal';
import {
  SALON_SERVICES_SEARCH,
  Customer,
  OFFERS,
  SalonHour,
  CityInfo
} from "./../../providers/SalonAppUser-Interface";
import { config } from "./../../providers/config/config";
import { Component, ViewChild } from "@angular/core";
import {
  NavController,
  NavParams,
  LoadingController,
  ViewController,
  Platform,
  Nav,
  Events,
  MenuController,
  App,
  AlertController
} from "ionic-angular";
import { MainHomePage } from "../main-home/main-home";
import { timer } from "rxjs/observable/timer";
import { SalonDetailsPage } from "../salon-details/salon-details";
import { AllOffersPage } from "../all-offers/all-offers";
import { CityListingPage } from "../city-listing/city-listing";
import { Storage } from "@ionic/storage";
import { GloballySharedData } from "../../providers/global-service/global-service";
import { TabsPage } from "../tabs/tabs";
import { NgZone } from "@angular/core";
import { last } from "rxjs/operators";
import { GlobalProvider } from "../../providers/global/global";
import {
  NativePageTransitions,
  NativeTransitionOptions
} from "@ionic-native/native-page-transitions";
import "rxjs/add/operator/retrywhen";
import "rxjs/add/operator/delay";
import "rxjs/add/operator/scan";
import { SqliteDbProvider } from "../../providers/sqlite-db/sqlite-db";
import { GoogleAnalytics } from "../../../node_modules/@ionic-native/google-analytics";
import { PopoverController } from "ionic-angular";
import { NOPSO_Header_OPTIONS, Salon } from '../../providers/SalonAppUser-Interface';
import { AppointmentsCardPage } from "../appointments-card/appointments-card";
import { AppMessages } from "../../providers/AppMessages/AppMessages";
import { Content } from "ionic-angular";
import { LoginSignUpPage } from "../login-sign-up/login-sign-up";
import { ConfigModal } from "../../providers/ConfigModal/ConfigModal";
import * as $ from "jquery";
import { AddAppointmentPage } from '../add-appointment/add-appointment';
import { SlotsViewPage } from "../slots-view/slots-view";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  public ALLOFFERS: OFFERS[];
  @ViewChild(Nav) nav: Nav;
  @ViewChild(Content) content: Content;
  @ViewChild("scroll") scroll: any;
  options: NativeTransitionOptions = {
    direction: "left",
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 0
  };
  public sal_ids = ''
  public tech_ids = ''
  public isPushToSlotView = false
  public search_kwyword = "general search"
  public reRequestCounter = 0;
  public slideOutNow = false;
  public isFavourites = false;
  public infiniteScroll: any;
  public nopsoHeaderOptions: NOPSO_Header_OPTIONS;
  public allOffers: OFFERS[];
  public offersForListing: OFFERS[];
  public subStyleImageName: string;
  public pinImage: string;
  public subStyleName: string;
  public txtSearchSalon: string;
  public serverSearchlbl: string = "Go";
  public unregisterBackButtonAction: any;
  public noSalonFound = false;
  public allSalon: Salon[] = [];
  public techTypeSalons: Salon[] = [];
  public CityInfo: CityInfo[] = [];
  public SalonHoursAll: SalonHour[];
  public dataFetchedFromLocal = false
  public salonsFetched = [];
  public allSalSerSubCat = [];
  public allSalonBackUp = [];
  public salonImagesURL = config.salonImgUrl;
  public techImagesURL = config.TechImageURL;
  public subStyleImagesBaseURL = config.StylesImagesURL;
  public defaultSalonPicName = "default.png";
  public showTagsPopOver = false;
  public myCustomer: Customer;
  public selectedSalon: Salon
  public salonForComponent: Salon
  selectedServicesObjects = []
  public offersObjects: OFFERS[] = [];
  public rescheduleAppointment: Tech_appointments
  techSelected: Sal_Techs;
  selectedTech: Sal_Techs;
  public sp_name = "";
  public pageTitle = "";
  public shouldCitySearch = false
  public cityname = ""
  public SalonsServices: SalonServices[];
  public sal_id = 0;
  public distance = 0;
  public distanceUnit = "";
  public offsetDb = 0
  public allTechs: Sal_Techs[] = [];
  public myyAllTechsTemp: Sal_Techs[] = [];
  docsCall = 1
  docsOffset = 0
  docsLimit = 8
  public lastFetchDate: any
  public alertShown: boolean = false;
  public isServerSearching = false;
  constructor(
    public techModal: TechnicianModal,
    public sqlProvider: SqliteDbProvider,
    public salonModal: SqliteDbProvider,
    private zone: NgZone,
    public loadingController: LoadingController,
    public events: Events,
    private menu: MenuController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public platform: Platform,
    public globalSearch: GloballySharedData,
    private viewCtrl: ViewController,
    private nativePageTransitions: NativePageTransitions,
    public serviceManager: GlobalProvider,
    public keyboard: Keyboard,
    public salonServicesModal: SalonServicesModal,
    public customerModal: CustomerModal,
    private sqlite: SQLite,
    private ga: GoogleAnalytics,
    public popoverCtrl: PopoverController,
    public appCtrl: App,
    public geolocation: Geolocation,
    public alertCtrl: AlertController,
    public globalService: GloballySharedData,
    public configModal: ConfigModal,
  ) {
    this.sp_name = this.navParams.get('sp_name')
    this.shouldCitySearch = this.navParams.get('isFromCityListing')
    this.cityname = this.navParams.get('cityname')
    if (this.sp_name) {
      this.search_kwyword = this.sp_name
      this.pageTitle = this.sp_name
    } else if (this.cityname) {
      this.search_kwyword = this.cityname
      this.pageTitle = "Doctors in " + this.cityname
    } else {
      this.pageTitle = "Find a Doctor"
    }
    this.txtSearchSalon = "";
    this.distanceUnit = this.globalSearch.distance_unit;

  }
  private getDoctorsForListingFromDB() {
    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonModal.getAllSalonsByTypeFromDB('0', this.cityname, this.sp_name).then(salons => {
          if (salons && salons.length > 0) {
            this.allSalon = salons;
            this.noSalonFound = false;
            this.offsetDb = salons.length;
            salons.forEach(sal => {
              if (sal.tech_id !== '0') {
                this.tech_ids += sal.tech_id + ',';
              }
              this.sal_ids += sal.sal_id + ',';
            });
            this.tech_ids = this.tech_ids.slice(0, this.tech_ids.length - 1);
            this.sal_ids = this.sal_ids.slice(0, this.sal_ids.length - 1);
            /** one we have data sources then we request to feth any modified or new doctors */
            this.sqlProvider.getSalMaxDate(this.search_kwyword).then(dateMax => {
              this.lastFetchDate = dateMax;
              this.getDoctorsAndTheirLocationsFromServer();
            });
          }
          else {
            /* in case no doctors found then we query from server to fetch doctors */
            this.lastFetchDate = '';
            this.getDoctorsAndTheirLocationsFromServer();
            this.noSalonFound = true;
          }

        });
      }
    });
  }
public salonType = {'clinc':'2', 'singleDoctor':'1'}
  getDoctorsAndTheirLocationsFromServer() {
    this.allSalonsIdsToDeleteDuplicate = ''
    if (!this.lastFetchDate || (this.lastFetchDate && this.lastFetchDate.trim().length === 0)) {
      this.lastFetchDate = ''
    }
    let cust_id = null
    if (this.myCustomer) {
      cust_id = this.myCustomer.cust_id
    }
    const params = {
      service: btoa('lst_salons'),
      sal_last_fetched: btoa(this.lastFetchDate),
      app_version: btoa("5.0"),
    }
    if (this.cityname) {
      params['keywords'] = btoa(this.cityname)
    }
    if (this.sp_name) {
      params['keywords'] = btoa(this.sp_name)
    }
    if (!this.lastFetchDate || (this.lastFetchDate && this.lastFetchDate.trim().length === 0)) {
      params['limit'] = btoa(this.docsLimit.toString())
      params['offset'] = btoa(this.docsOffset.toString())
      if (this.noSalonFound) {
        if (this.docsCall === 1) this.serviceManager.showProgress("Fetching doctors...")
      }
    } else {
      this.docsCall = -1
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
        /**
         * first deleting in active slaons
         * then deleting in active techs
         * then saving last fetch date in db
         */

          this.salonModal.deleteMultipleSalon(res.inactive_salons)

          this.salonModal.deleteInActiveTechFromSalonTable(res.inactive_techs)

          if (this.docsCall === 1) this.serviceManager.stopProgress()
          if (!res.salons && !res.techs) {
            this.lastFetchDate = res.response_datetime
            this.savePageModifyDateToDb(res.response_datetime)
            return
          } else {
            if (res.salons.length === 0 && res.techs.length === 0) {
              this.lastFetchDate = res.response_datetime
              this.savePageModifyDateToDb(res.response_datetime)
              this.docsCall = -1
              return
            }
          }
          if (this.docsCall <= 3 && res.salons != undefined && res.techs != undefined) {
            this.allSalonBackUp = []
            this.techTypeSalons = []
            let sal_type = this.salonType.singleDoctor
            // if (!this.allSalon || this.allSalon.length === 0) {
            //   this.allSalon = res.salons
            //   this.offsetDb = this.allSalon.length
            // }
            if (!res.salons || res.salons.length === 0) {
              sal_type = this.salonType.clinc
              this.allSalonBackUp = res.techs
            } else {
              this.techTypeSalons = res.techs
              this.allSalonBackUp = res.salons
            }

            if (sal_type === this.salonType.singleDoctor) {
              this.allSalonBackUp.forEach(salon => {
                this.allSalonsIdsToDeleteDuplicate += salon.sal_id + ','
              });
              this.allSalonsIdsToDeleteDuplicate = this.allSalonsIdsToDeleteDuplicate.slice(0, this.allSalonsIdsToDeleteDuplicate.length - 1)
            } else {
              let tech_id = ''
              this.allSalonBackUp.forEach(salon => {
                tech_id += salon.tech_id + ','
              });
              tech_id = tech_id.slice(0, tech_id.length - 1)
              this.salonModal.deleteInActSalonsTech(tech_id)
            }

            if (this.allSalonsIdsToDeleteDuplicate.length > 0) {
              this.allSalonsIdsToDeleteDuplicate = this.allSalonsIdsToDeleteDuplicate + ',' + res.inactive_salons
            } else {
              this.allSalonsIdsToDeleteDuplicate = res.inactive_salons
            }

            this.salonModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.salonModal.deleteMultipleSalon(this.allSalonsIdsToDeleteDuplicate).then(res1 => {

                  this.salonModal.insert_salon_table(this.allSalonBackUp, sal_type).then(() => {
                    if (!this.allSalon || this.allSalon.length === 0) {
                      this.getDoctorsForListingFromDB()
                    }
                    if (sal_type == this.salonType.singleDoctor) {
                      this.saveTechTypeSalon()
                    } else {
                    }

                    switch (this.docsCall) {
                      case 1:
                        if (sal_type === this.salonType.singleDoctor) {
                          this.getDoctorsForListingFromDB

                        }
                        this.docsCall += 1
                        this.docsOffset = 8
                        this.docsLimit = 500
                        this.getDoctorsAndTheirLocationsFromServer()
                        break;
                      case 2:
                        this.docsCall += 1
                        this.docsLimit = 500
                        this.docsOffset = 10
                        this.getDoctorsAndTheirLocationsFromServer()
                        break;
                      case 3:
                        this.docsLimit = 500
                        this.docsOffset += 500
                        this.savePageModifyDateToDb(res.response_datetime)
                        if (res.salons && res.salons >= 500) {
                          this.getDoctorsAndTheirLocationsFromServer()
                        }
                        else {
                          this.lastFetchDate = res.response_datetime
                        }
                      case -1:
                        break;
                      default:
                        break;
                    }
                  });
                })
              }
            })
          } else {
            console.log('inElseBlock')
          }
        },
        (error) => {
          this.serviceManager.stopProgress()
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        })
  }
  
  savePageModifyDateToDb(date) {
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        let dateObject = {
          sal_search_keyword: this.search_kwyword,
          sal_modify_datetime: date,
        }
        this.sqlProvider.save_into_sal_date(dateObject).then(categoryInsertedSuccessfully => {
        })
      }
    })
  }
  saveTechTypeSalon() {
    if (this.techTypeSalons && this.techTypeSalons.length > 0) {
      let techIds = ''
      this.techTypeSalons.forEach(salon => {
        techIds += salon.tech_id + ','
      });
      techIds = techIds.slice(0, techIds.length - 1)
      this.salonModal.deleteInActSalonsTech(techIds).then(res1 => {
        if (this.techTypeSalons) {

          this.salonModal.insert_salon_table(this.techTypeSalons, '2').then((res) => {
            // if (this.docsCall === 1 || this.docsCall === -1) {
            // this.getDocsFromDbOnServerResponce();
            // }
          })
        }
      })
    }
  }
  wentToLoadMore = false;
  noMoreContentToLoad = false
  loadMoreSalons(infiniteScroll) {
    console.log('loadMoreSalons');
    if (this.wentToLoadMore || this.noMoreContentToLoad) {
      console.log('going to dismiss load more');
      if (infiniteScroll) {
        try {
          infiniteScroll.complete();
        } catch (ex) { }
      }
      return;
    }
    console.log('loading more salonsss');
    this.infiniteScroll = infiniteScroll;
    this.getPostFromDb();
  }
  getPostFromDb() {
    console.log('getting post from db');
    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonModal
          .getAllSalonsByTypeFromDB(this.offsetDb, this.cityname, this.sp_name)
          .then(
            res => {
              console.log('response of db');
              this.doRestOfThings(res)
            },
            error => { }
          );
      }
    });
  }
  doRestOfThings(res) {
    if (res && res.length > 0) {

      this.allSalon = this.allSalon.concat(res)
      res.forEach(sal => {
        if (sal.tech_id !== '0') {
          this.tech_ids += sal.tech_id + ','
        }
        this.sal_ids += sal.sal_id + ','
      });
      this.tech_ids = this.tech_ids.slice(0, this.tech_ids.length - 1)
      this.sal_ids = this.sal_ids.slice(0, this.sal_ids.length - 1)
      this.noSalonFound = false;
      this.infiniteScroll.complete()
      if (this.allSalon && this.allSalon.length > 0) {
        this.offsetDb = this.allSalon.length;
      }
    } else {
      this.noSalonFound = true;
    }
    if (res && res.length < 8) {
      this.infiniteScroll.enable(false)
    } else {
    }
    timer(4000).subscribe(() => {
      this.wentToLoadMore = false
    })
  }
  public getSalDistance(val) {
    if (val.length > 3) {
      return val.substring(0, 3) + " miles";
    }
  }
  ionViewWillEnter() {
    if(!this.allSalon || this.allSalon.length === 0) this.getDoctorsForListingFromDB();
    this.btnClearSearchTapped()
    this.isPushToSlotView = false
  }
  ionViewDidEnter() {
    this.getAndSaveAppConfig()
    this.initializeBackButtonCustomHandler();
  }
  ionViewDidLoad() {
    this.allOffers = [];
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal.getAllSalonOffers().then(
          offers => {
            this.allOffers = offers;
            this.showSalonDetail();
          },
          error => {
            this.showSalonDetail();
          }
        );
      }
    });
  }
  showSalonDetail() {
    this.showTagsPopOver = false;
    let objPin = this.serviceManager.getFromLocalStorage(
      this.serviceManager.OBJ_PIN
    );
    let objSubStyle = this.serviceManager.getFromLocalStorage(
      this.serviceManager.OBJ_SUB_STYLE
    );
    if (objPin && objSubStyle) {
      this.pinImage = objPin.imageUrl;
      this.subStyleName = objSubStyle.ssc_name;
    }
    this.initializeBackButtonCustomHandler();
  }
  public latitude = '31.47203'
  public longitude = '74.38336'
  getOfferBySalId(Id: string) {
    var found = this.allOffers.find(function (element) {
      return element.sal_id == Id;
    });
    return found;
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  shouldBeginEditing(value) { }
  endEndEditing(value) {
  }
  isSearchResponseBack = true;
  shouldChangeCharacter(search) {
    let searchValue: string = search;
    if (searchValue.trim().length === 0) {
      this.filteredTags = [];
      this.showTagsPopOver = false;
      this.txtSearchSalon = "";
      this.keyboard.close();
      return;
    }
    searchValue = searchValue.toLowerCase();
    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonModal.searchSalonsByTypeFromDB(this.globalSearch.sty_id, searchValue).then(searchedResult => {
          this.isSearchResponseBack = false;
          if (searchedResult && searchedResult.length > 0) {
            this.filteredTags = [];
            this.filteredTags = searchedResult;
            if (this.filteredTags && this.filteredTags.length > 0) {
              this.showTagsPopOver = true;
            }
          } else {
            this.filteredTags = [];
            this.showTagsPopOver = false;
          }
        });
      }
    })
  }
  handleReturnkey() {
    this.keyboard.close();
  }
  ifPromotionMoreThan1(salId, salon) {
    this.offersForListing = [];
    this.allOffers.forEach(element => {
      if (element.sal_id == salId) {
        this.offersForListing.push(element);
      }
    });
    if (this.offersForListing.length > 1) {
      return true;
    } else {
      return false;
    }
  }
  getPromotions(salId) {
    let promotions = this.getOfferBySalId(salId);
    if (promotions == null) {
      return "";
    } else {
      return promotions.so_title;
    }
  }
  btnAllOfferTaped(salId, sal_name, sal_pic) {
    this.offersForListing = [];
    this.allOffers.forEach(element => {
      if (element.sal_id == salId) {
        this.offersForListing.push(element);
      }
    });
    this.navCtrl.push(AllOffersPage, {
      offersForListing: this.offersForListing,
      sal_name: sal_name,
      sal_pic: sal_pic
    });
  }
  btnBackTapped() {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(HomePage);
    }
  }
  openDetailsPage(salon, event) {
    event.stopPropagation();
    let offerType;
    let promotions = this.getOfferBySalId(salon.sal_id);
    if (promotions) {
      offerType = promotions.so_title;
    }
    if (salon.sal_type == this.salonType.clinc) {
      this.sqlProvider.getSalonDetailsFrom(salon.sal_id).then(selectedSalon => {
        if (selectedSalon) {
          this.selectedSalon = selectedSalon
          this.salonForComponent = selectedSalon
        } else {
          this.serviceManager.makeToastOnFailure(AppMessages.msgSalonisNotActive)
        }
      })
      this.onTechTap(salon.sal_id, salon.tech_id)
    } else {
      this.nativePageTransitions.slide(this.options);
      this.navCtrl.push(AddAppointmentPage, {
        isServerSearch: false,
        selectedSalon: salon,
        offerType: offerType,
        sal_id: salon.sal_id,
        isFromBookAppointment: true,
      });
    }
  }
  openDetailsPageSearch(salon) {
    let offerType;
    let promotions = this.getOfferBySalId(salon.sal_id);
    if (promotions) {
      offerType = promotions.so_title;
    }
    this.nativePageTransitions.slide(this.options);
    this.navCtrl.push(AddAppointmentPage, {
      isServerSearch: true,
      selectedSalon: salon,
      offerType: offerType,
      sal_id: salon.sal_id
    });
  }
  getSalonNameFirstCharacte(salonName: string): string {
    if (salonName.trim().length === 0) {
      return salonName;
    }
    var shortSalonName = "";
    let charCount = 0;
    let SplitSalonNames = salonName.split(" ");
    SplitSalonNames.forEach(element => {
      let salName: string = element;
      if (charCount < 4 && salName.trim().length > 0) {
        if (
          charCount === 3 &&
          salName.length === 1 &&
          SplitSalonNames.length > 3
        ) {
        } else {
          shortSalonName += salName.slice(0, 1);
          charCount += 1;
        }
      }
    });
    return shortSalonName.toLocaleUpperCase();
  }
  private customHandleBackButton(): void {
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(HomePage);
      }
    }
  }
  removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }
  localSearchArr = [];
  SALON_SERVICE_SEARCH: SALON_SERVICES_SEARCH[] = [];
  SQL_TABLE_SEARCh = "search";
  filteredTags = [];
  public highlight(SubStyleName: string) {
    if (!this.txtSearchSalon) {
      return SubStyleName;
    }
    return SubStyleName.replace(
      new RegExp(this.txtSearchSalon, "gi"),
      match => {
        return '<span class="highlightText">' + match + "</span>";
      }
    );
  }
  btnStyleTapped(objSearch, index) {
    let objSubStyle: SALON_SERVICES_SEARCH = objSearch;
    if (!objSubStyle) {
      return;
    }
    this.showTagsPopOver = false;
    if (objSubStyle.keyword.length == 0) {
      this.serviceManager.makeToastOnFailure("Tag Name is missing");
      return;
    }
    this.txtSearchSalon = objSubStyle.keyword;
    let type = objSubStyle.type;
    if (type == "service") {
      let sal_ids = "";
      let index = 0;
      let salonsServices = [];
      this.salonModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.salonServicesModal.getSalonServicesBySerId(objSubStyle.refer_id).then(res => {
            salonsServices = res;
            salonsServices.forEach(element => {
              if (index == 0) {
                sal_ids = element.sal_id;
              } else {
                sal_ids = sal_ids + ", " + element.sal_id;
              }
              index = index + 1;
            });
            this.salonModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.salonModal
                  .getSalonsListingFromSqliteDB(sal_ids)
                  .then(res => {

                    this.allSalon = []

                    this.allSalon = res;
                    this.allSalonBackUp = this.allSalon;
                  });
              }
            });
          });
        }
      });
    } else if (type == "salon") {
      this.salonServicesModal.getSalonServices;
      let sal_id = objSubStyle.refer_id;
      this.salonModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.salonModal.getSalonDetailsFrom(sal_id).then(mySalon => {
            if (mySalon) {
              this.nativePageTransitions.slide(this.options);
              this.navCtrl.push(AddAppointmentPage, {
                selectedSalon: mySalon,
                isFromBookAppointment: true,
                sal_id: mySalon.sal_id
              });
            }
          });
        }
      });
    }
  }
  getCustomer() {
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(
          customer => {
            if (customer) {
              this.myCustomer = customer;
            }
          },
          error => {
          }
        );
      }
    });
  }
  isScrolledToBottom = false;
  scrollChange = 0
  scrollHandler(event) {
    if ((!this.allSalon && this.allSalon.length <= 4)) {
      return
    } else {
      let elem: HTMLElement = this.content._scrollContent.nativeElement
      if (event.directionY === 'down') {
        this.scrollChange = event.scrollTop
        if (event.scrollTop > 120) {
          elem.setAttribute("style", "margin-top: 0px !important;");
          this.zone.run(() => {
            this.slideOutNow = true;
          });
        }
      } else {
        this.zone.run(() => {
          let myChange = this.scrollChange - event.scrollTop
          if (myChange > 100) {
            elem.setAttribute("style", "margin-top: 100px !important;");
            this.zone.run(() => {
              this.slideOutNow = false
            });
          }
        });
      }
      this.keyboard.close()
      if (event.scrollTop <= 0) {
        elem.setAttribute("style", "margin-top: 100px !important;");
      }
    }
  }
  searchOverInternet() {
  }
  public allSalonsIdsToDeleteDuplicate = ''
  searchDataOverServer() {
    let loading = this.loadingController.create({
      content: "Fetching Doctors, Please Wait..."
    });
    loading.present();
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDateTime()),
      service: btoa("search_salons"),
      keywords: btoa(this.txtSearchSalon)
    };
    this.serviceManager
      .getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })
      .subscribe(
        response => {
          loading.dismissAll();
          if (response.salons) {
            this.salonsFetched = response.salons;
          }
          if (this.salonsFetched && this.salonsFetched.length > 0) {
            this.salonModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.salonModal.deleteMultipleSalon(this.allSalonsIdsToDeleteDuplicate).then(res => {
                  this.salonModal.insert_salon_table(this.salonsFetched, '2').then(() => {
                    this.shouldChangeCharacter(this.txtSearchSalon.trim())
                  });
                });
              }
            });
          }
        },
        error => {
          loading.dismiss();
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );
          console.log("something went wrong", error);
        }
      );
  }
  calCulateSalHours() {
    this.SalonHoursAll = [];

    if (!this.allSalon) this.allSalon = this.allSalonBackUp
    this.allSalon.forEach(salon => {
      let sal_Id = salon.sal_id;
      let sal_hours = salon.sal_hours1;
      let salonHours = {};
      let salonHoursKeys = [];
      salonHoursKeys = Object.keys(sal_hours);
      salonHours = sal_hours;
      let temp = {};
      salonHoursKeys.forEach(element => {
        let split = sal_hours[element].split("&");
        if (split[0] == split[1]) {
          temp[element] = "Off";
        } else {
          temp[element] =
            this.convert24Hrto12Hr(split[0]) +
            " - " +
            this.convert24Hrto12Hr(split[1]);
        }
      });
      salonHours = temp;
      salonHoursKeys.forEach(dayKey => {
        let salonHoursObject = {
          sal_id: Number(sal_Id),
          sal_hours: salonHours[dayKey],
          sal_day: dayKey
        };
        this.SalonHoursAll.push(salonHoursObject);
      });
    });
    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        if (this.SalonHoursAll && this.SalonHoursAll.length > 0) {
          this.salonModal.saveIntoSalHoursTable(this.SalonHoursAll).then(isDataInserted => {
          });
        }
      }
    });
  }
  convert24Hrto12Hr(time) {
    if (!time) {
      return;
    }
    time = time
      .toString()
      .match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) {
      time = time.slice(1);
      time[5] = +time[0] < 12 ? " AM" : " PM";
      time[0] = +time[0] % 12 || 12;
    }
    time[0] = this.padToTwo(time[0]);
    return time.join("");
  }
  padToTwo(number) {
    if (number <= 9) {
      number = ("0" + number).slice(-2);
    }
    return number;
  }
  getAndSetSalonOffer(salons) {
    let so_ids = "";
    this.ALLOFFERS = [];
    salons.forEach(element => {
      if (element.offers !== null) {
        for (let index = 0; index < element.offers.length; index++) {
          const item = element.offers[index];
          this.ALLOFFERS.push(item);
        }
      }
    });
    this.saveSalonOffers(this.ALLOFFERS);
  }
  saveSalonOffers(ALLOFFERS) {
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        if (ALLOFFERS != undefined && ALLOFFERS != null) {
          this.salonServicesModal.insert_salon_offer(ALLOFFERS);
        }
      }
    });
  }
  insertSalonServices(salonIds) {
    if (!this.salonsFetched) { return; }
    this.SalonsServices = [];
    this.salonsFetched.forEach(salon => {
      this.SalonsServices = this.SalonsServices.concat(salon.services);
    });
    if (this.SalonsServices && this.SalonsServices.length > 0) {
      this.serviceManager.setInLocalStorage(
        this.serviceManager.NEAREST_SALON_SERVICES,
        this.SalonsServices
      );
    }
  }
  getSalonIds(response) {
    let salonIds = "";
    let i = 0;
    response.forEach(element => {
      salonIds += element.sal_id + ",";
    });
    salonIds = salonIds.slice(0, salonIds.length - 1);
    return salonIds;
  }
  roundRatingUpToOneDecimal(ratingValue) {
    return Math.round(ratingValue).toFixed(1);
  }
  public deviceType = ''
  savePublicUser() {
    let token = this.serviceManager.getFromLocalStorage(this.serviceManager.GCM_TOKEN);
    if (this.platform.is('ios')) {
      this.deviceType = "1";
    }
    if (this.platform.is('android')) {
      this.deviceType = "2";
    }
    const params = {
      service: btoa('save_public_user'),
      pu_device_id: btoa(token),
      pu_device_type: btoa(this.deviceType),
      pu_lat: btoa(this.latitude.toString()),
      pu_lng: btoa(this.longitude.toString()),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          let publicUserID = 0
          publicUserID = res.pu_id
          !publicUserID ? publicUserID = 0 : ''
          this.myCustomer = {};
          this.myCustomer.cust_id = publicUserID.toString()
          this.serviceManager.setInLocalStorage(this.serviceManager.PUBLIC_USER_ID, res.pu_id)
        },
        (error) => {
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        },
        () => {
        }
      )
  }
  getSalonIdsAndInsertSalonsToSearchTable(response, inactive_salons_ids) {
    !inactive_salons_ids ? inactive_salons_ids = '' : ''
    this.SALON_SERVICE_SEARCH = []
    let salonIds = '';
    let i = 0;
    response.forEach(element => {
      let item = {
        keyword: element.sal_name,
        type: "salon",
        refer_id: element.sal_id,
        image_url: element.sal_pic
      }
      this.SALON_SERVICE_SEARCH.push(item)
      salonIds += element.sal_id + ','
    });
    salonIds = salonIds.slice(0, salonIds.length - 1)
    if (salonIds.length > 0) {
      salonIds = salonIds + "," + inactive_salons_ids;
    } else {
      salonIds = inactive_salons_ids;
    }
    this.salonModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        if (salonIds !== undefined && salonIds !== null) {
          this.salonModal.deleteSearchKeyWordsBySalId(salonIds, 'salon').then(isDataInserted => {
          })
        }
        if (this.SALON_SERVICE_SEARCH !== undefined && this.SALON_SERVICE_SEARCH !== null && this.SALON_SERVICE_SEARCH.length > 0) {
          this.salonModal.InsertInTblSearch(this.SALON_SERVICE_SEARCH).then(isDataInserted => {
          })
        }
      }
    })
    return salonIds;
  }
  public alert
  presentConfirm() {
    this.alert = this.alertCtrl.create({
      title: 'Confirm exit....',
      message: 'Do you want to exit app?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.alertShown = false;
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });
    this.alert.dis
    this.alert.present().then(() => {
      this.alertShown = true;
    });
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      if (this.menu.isOpen()) {
        this.menu.close();
      } else {
        this.customHandleBackButton();
      }
    }, 10);
  }
  getAndSaveAppConfig() {
    let _appConfig = this.serviceManager.getFromLocalStorage('isAppConfigs')
    if (_appConfig && _appConfig.currency) {
      this.globalService.currencySymbol = _appConfig.currency
      this.globalService.distance_unit = _appConfig.distance_unit
    } else {
      this.configModal.getDatabaseState().subscribe(ready => {
        if (ready) {
          this.configModal.getAllAppConfig().then(data => {
            if (data && data.currency) {
              this.globalService.currencySymbol = data.currency
              this.globalService.distance_unit = data.distance_unit
            } else {
              this.getAppConfigFromServer()
            }
          })
        }
      })
    }
  }
  getAppConfigFromServer() {
    const params = {
      service: btoa('get_config'),
    }
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          let apConfigData: APP_CONFIG = res.config
          this.serviceManager.setInLocalStorage('isAppConfigs', apConfigData)
          if (apConfigData) {
            this.globalService.currencySymbol = apConfigData.currency
            this.globalService.distance_unit = apConfigData.distance_unit
          }
          this.configModal.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.configModal.InsertInAppConfigTable(apConfigData)
            }
          })
        },
        (error) => {
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        })
  }
  callToCust(cust_phone: string) {
    event.stopPropagation();
    window.open('tel:' + cust_phone);
  }
  onClickeventStopPropagation(event) {
    event.stopPropagation();
  }
  onModelChange(event) {
  }
  makeSalonFavoriteTapped() {
  }
  getSalonTechnician(last_fetch, sal_id, tech_id, progress) {
    if (progress) {
      this.serviceManager.showProgress('Fetching Doctors, Please Wait....')
    }
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      service: btoa("get_salon_techs"),
      sal_id: btoa(sal_id),
      last_fetched: btoa(last_fetch)
    };
    this.serviceManager
      .getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })
      .subscribe(
        response => {
          if (progress) {
            this.serviceManager.stopProgress();
          }
          this.handlResponseTech(response, sal_id, tech_id);
        },
        error => {
          if (progress) {
            this.serviceManager.stopProgress();
          }
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );
        }
      );
  }
  onTechTap(sal_id, tech_id) {
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider.getSalonTechnicianBySalId(sal_id).then(res => {
          this.myyAllTechsTemp = res;
          if (!this.myyAllTechsTemp || this.myyAllTechsTemp.length === 0) {
            this.sqlProvider.getSalonTechnicianLatUpdate(sal_id).then(lastupdateTime => {
              this.getSalonTechnician(lastupdateTime, sal_id, tech_id, true);
            });
          } else {
            this.techModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.myyAllTechsTemp.forEach(tech => {
                  this.techModal.getTechHoursFromDB(tech.tech_id).then(techHours => {
                    // console.log('tech_id ' + tech.tech_id + ' :tech hours array: ' + techHours.length);
                    tech.tech_Hours = techHours
                    return tech
                  });
                });
                this.allTechs = this.myyAllTechsTemp
                this.pushToSlotView(tech_id);
              }
            })
            this.sqlProvider.getSalonTechnicianLatUpdate(sal_id).then(lastupdateTime => {
              this.getSalonTechnician(lastupdateTime, sal_id, tech_id, false);
            });
          }
        })
      }
    })
  }
  pushToSlotView(tech_id) {
    if (!this.isPushToSlotView) {
      this.isPushToSlotView = true
      if (this.allTechs) {
        this.allTechs.forEach(tech => {
          if (Number(tech.tech_id) == Number(tech_id)) {
            this.selectedTech = tech;
          }
        });
      }
      this.navCtrl.push(SlotsViewPage, {
        selectedTech: this.selectedTech,
        salonForComponent: this.salonForComponent,
        selectedServicesObjects: this.selectedServicesObjects,
        selectedSalon: this.selectedSalon,
        rescheduleAppointment: this.rescheduleAppointment,
        offersObjects: this.offersObjects
      })
    } else {
      this.isPushToSlotView = false
    }
  }
  handlResponseTech(response, sal_id, tech_id) {
    if (!this.allTechs || this.allTechs.length === 0) {
      this.allTechs = response.sal_techs;
      if (this.allTechs && this.allTechs.length === 1) {
        if (this.allTechs && this.allTechs.length === 1) {
          this.allTechs.forEach(tech => {
            if (Number(tech.tech_id) === Number(tech_id)) {
              this.selectedTech = tech;
            }
          });
        }
      }
    } else {
    }
    // console.log('allTechs', JSON.stringify(this.allTechs))
    this.calculateTechHours(response.sal_techs)
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider.deleteSalTechs(response.update_ids).then(lastupdateTime => {
          if (response.sal_techs && response.sal_techs.length > 0) {
            this.sqlProvider.saveIntoSalTechTable(response.sal_techs).then(lastupdateTime => {
              this.pushToSlotView(tech_id);
              this.sqlProvider.getDatabaseState().subscribe(ready => {
                if (ready) {
                  this.sqlProvider.getSalonTotalTech(sal_id).then(totalTechs => {
                  });
                }
              });
            });
          } else {
            this.sqlProvider.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.sqlProvider.getSalonTotalTech(sal_id).then(totalTechs => {
                });
              }
            });
          }
        });
      }
    });
  }
  techHoursAll = []
  calculateTechHours(salonsTechnicians: Sal_Techs[]) {
    this.techHoursAll = [];
    salonsTechnicians.forEach(tech => {
      let tech_hours = tech.tech_work_time1;
      let salonHours = {};
      let salonHoursKeys = [];
      salonHoursKeys = Object.keys(tech_hours);
      salonHours = tech_hours;
      let temp = {};
      salonHoursKeys.forEach(element => {
        let split = tech_hours[element].split("&");
        if (split[0] == split[1]) {
          temp[element] = "Off";
        } else {
          temp[element] =
            this.convert24Hrto12Hr(split[0]) + "-" + this.convert24Hrto12Hr(split[1]);
        }
      });
      salonHours = temp;
      salonHoursKeys.forEach(dayKey => {
        let objTechHours: TECH_HOURS = {}
        objTechHours.sal_id = tech.sal_id
        objTechHours.tech_id = tech.tech_id
        objTechHours.tech_day = dayKey
        objTechHours.tech_hours = salonHours[dayKey]
        this.techHoursAll.push(objTechHours);
      });
    });
    // console.log('this.techHoursAll');
    // console.log(this.techHoursAll)
    this.techModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        if (this.techHoursAll && this.techHoursAll.length > 0) {
          this.techModal.InsertInToTechHoursTable(this.techHoursAll).then(isDataInserted => {
            if (!this.allTechs[0].tech_Hours) {
              this.techModal.getDatabaseState().subscribe(ready => {
                if (ready) {
                  this.allTechs.forEach(tech => {
                    this.techModal.getTechHoursFromDB(tech.tech_id).then(techHours => {
                      // console.log('tech_id ' + tech.tech_id + ' :tech hours array: ' + techHours.length);
                      tech.tech_Hours = techHours
                      return tech
                    });
                  });
                }
              })
            }
          });
        }
      }
    });
  }
  btnClearSearchTapped() {
    this.txtSearchSalon = ''
    this.shouldChangeCharacter('')
  }
}
