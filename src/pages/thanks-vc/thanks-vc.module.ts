import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ThanksVcPage } from './thanks-vc';

@NgModule({
  declarations: [
    ThanksVcPage,
  ],
  imports: [
    IonicPageModule.forChild(ThanksVcPage),
  ],
})
export class ThanksVcPageModule {}
