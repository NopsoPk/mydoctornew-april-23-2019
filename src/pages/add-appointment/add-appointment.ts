import { HomePage } from '../home/home';
import { Sal_Techs, Services, Tech_appointments, Salon, OFFERS, SALON_CARD_OPTIONS } from './../../providers/SalonAppUser-Interface';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Platform, Navbar } from 'ionic-angular';
import { SlotsViewPage } from '../../pages/slots-view/slots-view'
import { GlobalProvider } from '../../providers/global/global';
import { SalonServicesModal } from '../../providers/salon-services-modal/salon-services-modal';
import { SqliteDbProvider } from '../../providers/sqlite-db/sqlite-db'
import { config } from '../../providers/config/config';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { TechnicianModal } from '../../providers/TechnicianModal/TechnicianModal';
import { TECH_HOURS } from '../../providers/SalonAppUser-Interface';
import { GloballySharedData } from '../../providers/global-service/global-service';
@IonicPage()
@Component({
  selector: 'page-add-appointment',
  templateUrl: 'add-appointment.html',
})
export class AddAppointmentPage {
  @ViewChild(Navbar) navBar: Navbar;
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 0
  };
  baseUrl = config.TechImageURL;
  objSubStyle: any
  objPin: any
  pinImage: any
  rating: any
  dummyItemInList: any;
  public allTechs: Sal_Techs[] = [];
  public myyAllTechsTemp: Sal_Techs[] = [];
  rate: any;
  public selectedTechIndex = 0;
  firstCall = true
  cust_id: string;
  public selectedSalon: Salon
  public salonForComponent: Salon
  techSelected: Sal_Techs;
  _services: any[]
  servicesCountByCategory = [];
  _mainService: any[]
  salonServices: any[]
  subStyleServices = []
  servicesSortedByCat = []
  Categories = []
  selectedServicesObjects = []
  totalPrice = 0.0
  totalTime = 0
  selectedTech: Sal_Techs;
  public selectedTechID: number
  selectedTechServices: Services[]
  isAppoTechChanged = false
  public offersObjects: OFFERS[] = [];
  public unregisterBackButtonAction: any;
  public rescheduleAppointment: Tech_appointments
  public salonCardOption: SALON_CARD_OPTIONS;
  public totalTechs = 0
  public isenabledConfirmbutton: boolean = false;
  public offerType: any;
  constructor(
    public sqlProvider: SqliteDbProvider,
    public navCtrl: NavController,
    public toast: ToastController,
    public navParams: NavParams,
    public platform: Platform,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
    private salonServiceModal: SalonServicesModal,
    public salonModal: SqliteDbProvider,
    public techModal: TechnicianModal,
    public gsp: GloballySharedData,
  ) {
    this.selectedSalon = this.navParams.get("selectedSalon");
    this.salonForComponent = this.navParams.get('salonForComponent');
    this.rescheduleAppointment = this.navParams.get('rescheduleAppointment')
    this.selectedServicesObjects = this.navParams.get("selectedServicesObjects");
    this.salonForComponent = this.navParams.get("selectedSalon");
    this.selectedTechID = this.navParams.get("selectedTechID");
  }
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(AddAppointmentPage)
    }
  }
  ionViewCanEnter() {
  }
  ionViewWillEnter() {
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.sqlProvider.getSalonTechnicianBySalId(this.selectedSalon.sal_id).then(res => {
          this.myyAllTechsTemp = res;

          if (!this.myyAllTechsTemp || this.myyAllTechsTemp.length === 0) {
            this.sqlProvider.getSalonTechnicianLatUpdate(this.selectedSalon.sal_id).then(lastupdateTime => {
              this.getSalonTechnician(lastupdateTime, this.selectedSalon.sal_id, true);
            });
          } else {
            this.techModal.getDatabaseState().subscribe(ready => {
              if (ready) {
                this.myyAllTechsTemp.forEach(tech => {
                  if (this.selectedTechID && Number(tech.tech_id) === Number(this.selectedTechID)) {
                    this.selectedTech = tech
                  }
                  this.techModal.getTechHoursFromDB(tech.tech_id).then(techHours => {
                    console.log('tech_id ' + tech.tech_id + ' :tech hours array: ' + techHours.length);
                    tech.tech_Hours = techHours
                    return tech
                  });
                });
                this.allTechs = this.myyAllTechsTemp
                if (this.allTechs && this.allTechs.length === 1) {
                  this.OnTechSecltion(this.allTechs[0], 0)
                } else if (this.selectedTechID) {
                  let matched = false
                  this.allTechs.forEach((tech, index) => {
                    if (!matched && Number(tech.tech_id) === Number(this.selectedTechID)) {
                      this.selectedTech = tech
                      this.selectedTechIndex = index
                      matched = true
                    }
                  });
                  if (matched) {
                    this.OnTechSecltion(this.selectedTech, this.selectedTechIndex)
                  }
                }
                if (this.rescheduleAppointment) {
                  this.allTechs.forEach((tech, index) => {
                    if (Number(this.rescheduleAppointment.tech_id) === Number(tech.tech_id) || (this.selectedTechIndex && Number(this.selectedTechIndex) === Number(tech.tech_id))) {
                      this.OnTechSecltion(tech, index)
                    }
                  });
                }
                if (this.selectedTech) {
                  this.allTechs.forEach((tech, index) => {
                    if (Number(this.selectedTech.tech_id) === Number(tech.tech_id) || (this.selectedTechIndex && Number(this.selectedTechIndex) === Number(tech.tech_id))) {
                      this.OnTechSecltion(tech, index)
                    }
                  });
                }
              }
            })
            this.sqlProvider.getSalonTechnicianLatUpdate(this.selectedSalon.sal_id).then(lastupdateTime => {
              this.getSalonTechnician(lastupdateTime, this.selectedSalon.sal_id, false);
            });
          }
        })
      }
    })
    /* Finding selected to show to user */
    /* populating tech services when tech is found and identified*/
    this.salonForComponent = this.navParams.get("selectedSalon");
    let Salons: Salon[] = [];
    var _CardHeight = "315pt";
    Salons.push(this.salonForComponent);
    this.salonCardOption = {
      CardHeight: _CardHeight,
      calledFrom: config.SalonDetailPage,
      ShouldShowSalonImage: true,
      shouldScroll: false,
      shouldNotLoadSalonAgain: true,
      salon: Salons
    };
  }
  public currencySymbol = "";
  ionViewDidEnter() {
    this.currencySymbol = this.gsp.currencySymbol;
    this.initializeBackButtonCustomHandler();
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(HomePage)
    }
  }
  scrollTo(index) {
    this.selectedTechIndex = index;
  }
  goBack() {
    this.navCtrl.pop();
  }
  selectedTechDetail(selectedTechnician, index) {
    if (this.selectedTechIndex == index && !this.firstCall) {
      return;
    }
    this.firstCall = false
    this.selectedTechIndex = index;
    this.selectedTech = selectedTechnician;
    this.totalPrice = 0;
    this.totalTime = 0;
    this.selectedTech = selectedTechnician
    this.subStyleServices = []
    this.servicesSortedByCat = [];
    this.servicesCountByCategory = []
    this.Categories.forEach(cat => {
      let catgegory: Services = cat
      var count = 0
      this.servicesSortedByCat.forEach(ser => {
        let service: Services = ser
        if (catgegory.ssc_id === service.ssc_id) {
          count += 1
        } else {
          return
        }
      });
      this.servicesCountByCategory.push(count)
    });
    this.isAppoTechChanged = true
  }
  OnTechSecltion(objTech: Sal_Techs, techIndex) {
    if (objTech.isSelected === true) return
    this.allTechs.map(tech => {
      if (tech === objTech) {
        tech.isSelected = !tech.isSelected
      } else {
        tech.isSelected = false
      }
    })
    if (objTech.isSelected) {
      this.techSelected = objTech;
    } else {
      this.techSelected = null
    }
    this.selectedTech = objTech;
  }
  btnNextTappe() {
    if (!this.techSelected) {
      if (Number(this.selectedSalon.sal_register_as) < 2)
        this.serviceManager.makeToastOnFailure(AppMessages.msgSelectPracticeLocation)
      else this.serviceManager.makeToastOnFailure(AppMessages.msgSelectMakeArtist)
      return
    }
    this.nativePageTransitions.slide(this.options)
    if(!this.salonForComponent) this.salonForComponent = this.selectedSalon
    console.log(this.salonForComponent);
    
    this.navCtrl.push(SlotsViewPage, {
      selectedTech: this.selectedTech,
      salonForComponent: this.salonForComponent,
      selectedServicesObjects: this.selectedServicesObjects,
      selectedSalon: this.selectedSalon,
      rescheduleAppointment: this.rescheduleAppointment,
      offersObjects: this.offersObjects
    })
  }
  getSalonTechnician(last_fetch, sal_id, showProgress) {
    var params = {
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
      service: btoa("get_salon_techs"),
      sal_id: btoa(sal_id),
      last_fetched: btoa(last_fetch)
    };
    if (showProgress) this.serviceManager.showProgress('Please Wait....')
    this.serviceManager
      .getData(params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })
      .subscribe(
        response => {
          this.serviceManager.stopProgress()
          this.isenabledConfirmbutton = true;
          this.handlResponseTech(response, sal_id);
        },
        error => {
          this.serviceManager.stopProgress();
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );
        }
      );
  }
  handlResponseTech(response, sal_id) {

    if (!this.allTechs || this.allTechs.length === 0) { /** if no doctor or practice location availale then need to update datasource else just go to dump the doctors or practice location */
      this.allTechs = response.sal_techs;

      /**
       * one single doctor or practice location showing it selected.
       */
      if (this.allTechs && this.allTechs.length === 1) {
        this.OnTechSecltion(this.allTechs[0], 0)
      } else if (this.selectedTechID) {
        /** checking for  single doctor or practice location to show selected
         *  when more than one doctors are there 
         * */
        let matched = false
        this.allTechs.forEach((tech, index) => {
          if (!matched && Number(tech.tech_id) === Number(this.selectedTechID)) {
            this.selectedTech = tech
            this.selectedTechIndex = index
            matched = true
          }
        });
        if (matched) {
          /**
           * selecting now practice location or doctor  if found
           */
          this.OnTechSecltion(this.selectedTech, this.selectedTechIndex)
        }
      }
      /**
       * REschecule process is
       */
      if (this.rescheduleAppointment) {
        this.allTechs.forEach((tech, index) => {
          if (this.rescheduleAppointment.tech_id === tech.tech_id) {
            this.OnTechSecltion(tech, index)
          }
        });
      }
    }
    console.log('allTechs', JSON.stringify(this.allTechs))
    this.calculateTechHours(response.sal_techs)
    this.sqlProvider.getDatabaseState().subscribe(ready => {
      if (ready) {

        if (response.sal_techs && response.sal_techs.length > 0) {
          this.totalTechs = response.sal_techs.length;
          this.sqlProvider.saveIntoSalTechTable(response.sal_techs).then(lastupdateTime => {

            this.sqlProvider.getSalonTotalTech(sal_id).then(totalTechs => {
              this.totalTechs = totalTechs;
            });

          });
        } else {
          this.sqlProvider.getDatabaseState().subscribe(ready => {
            if (ready) {
              this.sqlProvider.getSalonTotalTech(sal_id).then(totalTechs => {
                this.totalTechs = totalTechs;
              });
            }
          });
        }
      }
    });
  }
  techHoursAll = []
  allSalon = []
  calculateTechHours(salonsTechnicians: Sal_Techs[]) {
    this.techHoursAll = [];
    salonsTechnicians.forEach(tech => {
      let tech_hours = tech.tech_work_time1;
      let salonHours = {};
      let salonHoursKeys = [];
      salonHoursKeys = Object.keys(tech_hours);
      salonHours = tech_hours;
      let temp = {};
      salonHoursKeys.forEach(element => {
        let split = tech_hours[element].split("&");
        if (split[0] == split[1]) {
          temp[element] = "Off";
        } else {
          temp[element] =
            this.convert24Hrto12Hr(split[0]) + "-" + this.convert24Hrto12Hr(split[1]);
        }
      });
      salonHours = temp;
      salonHoursKeys.forEach(dayKey => {
        let objTechHours: TECH_HOURS = {}
        objTechHours.sal_id = tech.sal_id
        objTechHours.tech_id = tech.tech_id
        objTechHours.tech_day = dayKey
        objTechHours.tech_hours = salonHours[dayKey]
        this.techHoursAll.push(objTechHours);
      });
    });

    this.techModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        if (this.techHoursAll && this.techHoursAll.length > 0) {
          this.techModal.InsertInToTechHoursTable(this.techHoursAll).then(isDataInserted => {
            if (this.allTechs && this.allTechs.length > 0 && !this.allTechs[0].tech_Hours) {
              this.techModal.getDatabaseState().subscribe(ready => {
                if (ready) {
                  this.allTechs.forEach(tech => {
                    this.techModal.getTechHoursFromDB(tech.tech_id).then(techHours => {
                      console.log('tech_id ' + tech.tech_id + ' :tech hours array: ' + techHours.length);
                      tech.tech_Hours = techHours
                      return tech
                    });
                  });
                }
              })
            }
          });
        }
      }
    });
  }
  convert24Hrto12Hr(time) {
    console.log(time)
    if (time === undefined || time === null) {
      this.navCtrl.pop();
      return;
    }
    time = time
      .toString()
      .match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) {
      time = time.slice(1);
      time[5] = +time[0] < 12 ? " AM" : " PM";
      time[0] = +time[0] % 12 || 12;
    }
    time[0] = this.padToTwo(time[0]);
    return time.join("");
  }
  padToTwo(number) {
    if (number <= 9) {
      number = ("0" + number).slice(-2);
    }
    return number;
  }
  public techHours: TECH_HOURS[] = []
  getTechHoursFromDB() {
    this.techModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.techModal.getTechHoursFromDB(this.selectedSalon.sal_id).then(res => {
          this.techHours = res;
        });
      }
    });
  }
  roundAmount(amount: number | string) {
    return Math.round(Number(amount));
  }
}