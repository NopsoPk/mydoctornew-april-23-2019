import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SlotsViewPage } from './slots-view';

@NgModule({
  declarations: [
    SlotsViewPage,
  ],
  imports: [
    IonicPageModule.forChild(SlotsViewPage),
  ],
})
export class SlotsViewPageModule {}
