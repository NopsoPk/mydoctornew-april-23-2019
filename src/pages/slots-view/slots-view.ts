import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { GlobalProvider } from './../../providers/global/global';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Platform, Navbar } from 'ionic-angular';
import { Tech_Slots, Sal_Techs, Tech_appointments, Services, SalonServices, Salon, OFFERS, SALON_PROMOTION, Category } from '../../providers/SalonAppUser-Interface';

import { Content } from 'ionic-angular';
import { MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConfirmBookingPage } from '../../pages/confirm-booking/confirm-booking';
import { retry } from 'rxjs/operators/retry';
import { concat } from 'rxjs/operator/concat';
import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { MainHomePage } from '../main-home/main-home';
import { SalonServicesModal } from '../../providers/salon-services-modal/salon-services-modal';
import { ViewController } from 'ionic-angular';
import { Customer } from '../../providers/SalonAppUser-Interface';
import { CustomerModal } from '../../providers/CustomerModal/CustomerModal';
import { Events } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { Keyboard } from '@ionic-native/keyboard';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-slots-view',
  templateUrl: 'slots-view.html',
})
export class SlotsViewPage {
  @ViewChild(Content) content: Content;
  valueforngif = true;

  @ViewChild(Navbar) navBar: Navbar;
  public appointmentTotalTime = 0
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation 
    fixedPixelsBottom: 0 // not to include this Bottom section in animation 
  };
  // params: any
  public unregisterBackButtonAction: any;
  allTechs: any;
  public calendarOption;
  selectedDateStr = "";
  existingAppo: Tech_appointments;
  dateToGetSlots: Date = new Date();
  // array = []
  techSlots: Tech_Slots[] = [];
  selectedSlot: Tech_Slots
  cust_id: string
  colorDay: Date;
  coloredDays = []
  objSubStyle: any
  objPin: any
  pinImage: any
  subStyleServicesObject: any
  public selectedTech: Sal_Techs;

  public selectedSalon: Salon
  public salonForComponent: Salon
  public rescheduleAppointment: Tech_appointments
  public salonOffers: SALON_PROMOTION[] = []
  public selectedPromotion: SALON_PROMOTION
  public salonOfferTitle = ''

  //customer information section
  customerProfileExist = false
  showCustomerProfile = false
  myCustomer: Customer
  firstname = '';
  email = '';
  PhoneNumber: '';
  responseJson: any[];
  response: string;
  location = '';
  deviceType: string;
  errorMessageName = '';
  errorMessageEmail = '';
  errorMessageLocation = '';
  isValidName = true;
  isValidEmail = true;
  isValidLocation = true;
  isValidLatLan = false;
  isGenderMale = true;
  customerGender: string
  postCode: string;
  longitude: any;
  latitude: any;
  emailExpression = '^[\w\.]+@([\w]+\.)+[A-Z]{2,7}$';
  //end customer information section
  constructor(
    public keyBoard: Keyboard,

    public navCtrl: NavController,
    public storage: Storage,
    public platform: Platform,
    public loader: LoadingController,
    public serviceManager: GlobalProvider,
    public salonServicesModal: SalonServicesModal,
    private toastCtrl: ToastController,
    private salonServiceModal: SalonServicesModal,

    private loadingController: LoadingController,

    private menu: MenuController,
    private navParams: NavParams,
    private viewCtrl: ViewController,
    public customerModal: CustomerModal,
    private nativePageTransitions: NativePageTransitions,
    public events: Events,
  ) {

    this.selectedTech = this.navParams.get('selectedTech');
    this.salonForComponent = this.navParams.get('salonForComponent');
    this.selectedServicesObjects = this.navParams.get('selectedServicesObjects')
    this.selectedSalon = this.navParams.get('selectedSalon')
    this.rescheduleAppointment = this.navParams.get('rescheduleAppointment')
    let offersObjects: OFFERS[] = this.navParams.get('offersObjects')

    this.calendarOption = {
      fontSize: 8,
      fontWeight: 600,
      numberOfDays: this.selectedSalon['sal_future_app_days'],
      calendarBackgroundColor: 'white',
      hieghtOfCalendar: '86pt',
      defaultColor: 'white',

      selectedDateColor: 'black',
      seletedDayColor: 'black',
      selectedMonthAndYearColor: 'white',
      unSelectedDateColor: 'rgb(150,150,150)',
      circleBorderColor: '1px solid #969696',
      circleBackgroundColor: 'white',
      sizeOfCircle: 18.7,

      selectedBackgroundColor: 'rgb(0, 144, 206)',
      selectedColor: 'white',
      selectedBorderColor: 'none',
      pomotionDaysColor: 'rgb(255,95,109)',
      offersObjects: offersObjects,

    };

    if (this.existingAppo != undefined) {
      let date = this.existingAppo.app_start_time.split(' ')[0];
      console.log('done from here...1');
      date = this.serviceManager.getStringFromDateWithFormat(new Date(date), 'MM/dd');
      console.log('done from here...2');
      this.coloredDays = [{
        d: date,
        background: 'red'
      }];
      console.log('done from here...3');
    }

    let $comp = this
    this.selectedDateStr = this.serviceManager.getStringFromDateWithFormat(new Date(), 'dd MMM');
    this.storage.get(this.serviceManager.CUSTOMER_DATA).then((value) => {
      console.log('LoginStatus', value);
      this.cust_id = value.cust_id;
      value ? true : false
    }).catch((Error) => {
      console.log('Error', 'in login');
    });

    let now = new Date();
    if (this.existingAppo != undefined) {
      let tempDate = this.existingAppo.app_start_time;
      tempDate = tempDate.replace(/-/g, '/');
      this.dateToGetSlots = new Date(tempDate);

    }

    this.techSlots = []

    if (this.existingAppo != undefined) {

      let tempDate = this.existingAppo.app_start_time;
      tempDate = tempDate.replace(/-/g, '/');
      this.calendarOption['appoDate'] = new Date(tempDate);
    }
    /** 
    else if (this.selectedSalon['sal_temp_enabled'] == "1") {
      var tomorrow = new Date();
      tomorrow.setDate(tomorrow.getDate() + 1);
      this.calendarOption['tomorrow'] = tomorrow;
      this.dateToGetSlots = tomorrow;
      this.calendarOption['appoDate'] = "N/A";
    }*/
    else {

      this.calendarOption['appoDate'] = "N/A";
    }
    this.subStyleServicesObject = []
  }

  scrollHandler(event) {
    if (!event) return
    let elem: HTMLElement = this.content._scrollContent.nativeElement
    if (event.directionY && event.directionY === 'down') {
      if (event.scrollTop > 300) {
        this.keyBoard.close()
      }
    } else {
    }
  }
  handleReturnkey() {
    this.keyBoard.close()
  }
  onPageScroll(event) {
    console.log(event.target.scrollTop);
  }
  ionViewDidEnter() {

    this.keyBoard.onKeyboardShow().subscribe(() => { this.valueforngif = false })
    this.keyBoard.onKeyboardHide().subscribe(() => { this.valueforngif = true })
    this.initializeBackButtonCustomHandler();
    this.salonOffers = []
    this.salonServicesModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.salonServicesModal.getSalonOffers(this.selectedSalon.sal_id).then(offers => {
          this.salonOffers = offers
          let selectedDayNumber = this.dateToGetSlots.getDay()
          if (selectedDayNumber === 0) {
            selectedDayNumber = 7
          }
          // console.log(JSON.stringify(this.salonOffers));
          if(this.salonOffers)
          this.salonOffers.forEach(salonOffer => {
            if (salonOffer.so_days.includes(selectedDayNumber.toString())) {
              this.salonOfferTitle = salonOffer.so_title
            }
          });
        }, error => {
        })
      }
    })
    this.getSalonDetailFromDb();
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }
  private customHandleBackButton(): void {
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(HomePage)
      }
    }
    // this.navCtrl.pop();
  }
  //end custom back button 
  ionViewWillEnter() {
    this.showCustomerProfile = false
    this.customerProfileExist = false

    this.getTechSlots();

    // this.getCustomer();
  }

  ionViewDidLoad() {
    // this.selectedServicesObjects.forEach(objService => {
    //   this.appointmentTotalTime += Number(objService.sser_time)
    // });
    // let objSubStyle = this.serviceManager.getFromLocalStorage(this.serviceManager.OBJ_SUB_STYLE)
    let objPin = this.serviceManager.getFromLocalStorage(this.serviceManager.OBJ_PIN)
    if (objPin) {
      this.pinImage = objPin.imageUrl
      this.objPin = objPin
    }
    if (this.techSlots == undefined) {
      return
    }
    if( this.techSlots)
    this.techSlots.forEach(element => {
      let slot: Tech_Slots = element
      if (slot == undefined) {
        console.log('slots is undefined');
        return
      }
      console.log('slots Object', slot);
    });

    // this.navBar.backButtonClick = (e: UIEvent) => {

    //   let options: NativeTransitionOptions = {
    //     direction: 'right',
    //     duration: 500,
    //     slowdownfactor: 3,
    //     slidePixels: 20,
    //     iosdelay: 100,
    //     androiddelay: 150,
    //     fixedPixelsTop: 0,
    //     fixedPixelsBottom: 0
    //   };
    //   if(this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    //   this.navCtrl.pop({
    //     animate: false,
    //     animation: 'ios-transition',
    //     direction: 'back',
    //     duration: 500,
    //   })
    // }

  }
  getTechSlots() {
    this.serviceManager.showProgress('Fetching Slots, Please Wait...')
    // let loading = this.loader.create({ content: "Fetching Slots, Please Wait..." });
    // loading.present();

    var params = {
      service: btoa('get_tech_slots'),
      tech_id: btoa(this.selectedTech.tech_id.toString()),
      app_duration: btoa(this.appointmentTotalTime.toString()),
      app_date: btoa(this.serviceManager.getStringFromDateWithFormat(this.dateToGetSlots, 'yyyyMMdd')),
      sal_app_after_hours: btoa(this.salonForComponent.sal_app_after_hours.toString()),
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime()),
    }

    this.selectedDateStr = this.serviceManager.getStringFromDateWithFormat(this.dateToGetSlots, 'dd MMM');
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })

      .subscribe(res => {

        this.techSlots = [];
        this.serviceManager.stopProgress();

        this.techSlots = res['tech_slots'];
        if( this.techSlots)
        this.techSlots.forEach(slot => {
          if (this.selectedSlot && this.selectedSlot.ts_number === slot.ts_number) {
            return slot.isSelected = true
          } else {
            return slot.isSelected = false
          }

        });
        if (this.techSlots) {

          if (this.existingAppo != undefined) {
            let firstSlot = this.existingAppo.app_slots.split(',')[0];
           
            for ( let slot = 0; slot < this.techSlots.length; slot++) {

              console.log('First Slot => ' + firstSlot);
              console.log(' Slots => ' + this.existingAppo.app_slots);
              console.log('All Slot => ' + this.techSlots[slot]['ts_number']);

              if (this.techSlots[slot]['ts_number'] == firstSlot) {
                // this.selectedSlot = slot;

                console.log('got the slot ... ' + this.techSlots[slot]['ts_number']);
                break;
              }
            }
          }

        } else {
          this.techSlots = []
        }

        this.content.scrollTo(0, 1).then(res => {
          this.content.resize()
          this.content.scrollTo(0, 0, 0).then(res => {
          })
        })
      },
        error => {
          this.serviceManager.stopProgress()
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
          console.log('something went wrong', error);

        });

  }

  getSlotFromattedTime(slotTime) {
    return this.serviceManager.get12HourFormatFrom24Hour(slotTime);
  }

  getSlotTimeWithoutSuffix(slotTime) {
    return this.serviceManager.get12HourFormatFrom24HourWithoutSuffix(slotTime);
  }

  getSlotAmPm(slotTime) {
    return this.serviceManager.get12HourFormatSuffix(slotTime);
  }

  slotDidSelected(objSlot, slot) {
    console.log('slotInfo',JSON.stringify(objSlot))
    this.techSlots.map(slot => {
      slot === objSlot ? slot.isSelected = !slot.isSelected : slot.isSelected = false
    })
    objSlot.isSelected ? this.selectedSlot = objSlot : this.selectedSlot = null
  }

  btnBookAppointmentTapped() {

    if (!this.selectedSlot || this.selectedSlot === null) {
      this.serviceManager.makeToastOnFailure(AppMessages.msgTimeSlotSelection)
      return;
    }

    this.nativePageTransitions.slide(this.options)
    this.navCtrl.push(ConfirmBookingPage, {
      selectedSalon: this.selectedSalon,
      salonForComponent: this.salonForComponent,
      selectedServicesObjects: this.selectedServicesObjects,
      selectedTech: this.selectedTech,
      selectedSlot: this.selectedSlot,
      rescheduleAppointment: this.rescheduleAppointment,
      selectedPromotion: this.selectedPromotion
    });

    // if (!this.customerProfileExist) {
    //   this.showCustomerProfile = true

    // } else {
    //customer exist tap to next screen 
    // this.goToConfirmBooking();
    // }

  }

  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'right',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };
    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options)
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(HomePage)
    }
  }
  dateClicked(date) {
    this.dateToGetSlots = date;
    this.selectedSlot = null
    this.getTechSlots();
    this.salonOfferTitle = null
    let selectedDayNumber = this.dateToGetSlots.getDay()
    if (selectedDayNumber === 0) {
      selectedDayNumber = 7
    }
    // console.log(JSON.stringify(this.salonOffers));
    if(this.salonOffers)
    this.salonOffers.forEach(salonOffer => {
      if (salonOffer.so_days.includes(selectedDayNumber.toString())) {
        this.salonOfferTitle = salonOffer.so_title
        this.selectedPromotion = salonOffer
      }
    });
  }
  //customer information section events

  // getCustomer() {
  //   console.log('came in get customer');
  //   this.customerModal.getDatabaseState().subscribe(ready => {
  //     if (ready) {
  //       this.customerModal.getCustomer().then(customer => {
  //         if (customer) {
  //           console.log('my customer FROM DB');
  //           console.log(JSON.stringify(customer));
  //           this.myCustomer = customer

  //           if (this.myCustomer.cust_name) {
  //             if (this.myCustomer.cust_name.length > 0) {
  //               this.customerProfileExist = true;
  //             }
  //           } else {
  //             this.customerProfileExist = false;
  //           }
  //         } else {
  //           this.customerProfileExist = false;
  //         }
  //       }, error => {
  //         console.log('error: while getting customer');
  //       })
  //     }
  //   })
  // }

  // OnValueEnterName(name) {
  //   this.isValidNameInput();
  // }
  // OnValueEnterEmail(email) {
  //   this.isValidEmailInput();
  // }
  // presentToast(message) {
  //   let toast = this.toastCtrl.create({
  //     message: message,
  //     duration: 5000,
  //     position: 'top',
  //     cssClass: "ToastClass",
  //   });
  //   toast.onDidDismiss(() => {
  //     console.log('Dismissed toast');
  //   });
  //   toast.present();
  // }
  // OnValueEnterLocation(location) {
  //   // this.isValidLocationInput();
  // }
  // OnCompleteActivation() {
  //   this.checkValidation();
  //   if (this.isValidEmail && this.isValidName) {

  //     // if(this.isValidLatLan){
  //     //   this.resgisterUser();
  //     // }else{
  //     //   this.serviceManager.makeToastOnFailure("Can't get your location, please check your internet connection!",0);

  //     // }
  //     this.resgisterUser();
  //   } else {
  //     //this.serviceManager.makeToastOnFailure("Can't get your location, please check your internet connection!"+this.latitude,0);
  //     console.log("Activation can't completes with errors");
  //   }
  // }
  // checkValidation() {

  //   this.isValidNameInput();
  //   this.isValidEmailInput();
  //   //  this.isValidLocationInput();
  //   // this.checkLocationFormat();
  //   this.checkEmailExpression();
  // }
  // isValidNameInput() {
  //   if (this.firstname.length == 0) {
  //     this.isValidName = false;
  //     this.errorMessageName = 'Please provide your name'
  //   } else {
  //     this.isValidName = true;
  //     this.errorMessageName = ''
  //   }
  // }

  //end customer information section events

  getSalonDetailFromDb() {

    this.salonServiceModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        // this.salonServiceModal.getSalonBySalId(this.selectedSalon.sal_id).then(salon => {
        this.salonServiceModal.getAllSalonServiceBySal(this.selectedSalon.sal_id).then(allServices => {

          this.salonServiceModal.getAllSalonServiceCategoriesBySalId(this.selectedSalon.sal_id).then(mainCategories => {
            if(mainCategories)
            mainCategories.forEach(category => {
              let ssc_id = category.ssc_id;
              let serviceObj = [];
              if(allServices)
              allServices.forEach(service => {
                if (ssc_id == service.ssc_id) {
                  serviceObj.push(service);
                }
              });
              return (category.services = serviceObj);
            });
            this.getAllDataFromLocalDb(mainCategories);
          });
        });

      }
    });
  }

  public isSalonActive = 0;
  public allServices: any[];
  public Categories: Category[] = [];
  public selectedServicesObjects: Services[] = [];
  // public selectedServicesObjects: SalonServices[]
  getAllDataFromLocalDb(Categories) {

    this.events.publish("gotSalonDetail", this.selectedSalon);

    this.isSalonActive = Number(this.selectedSalon.sal_status);
    this.Categories = Categories;

    if (this.rescheduleAppointment) {
      let appoServices = this.rescheduleAppointment.app_services;
      let appoSersArray = appoServices.split(",");
      let __selectedServicesObjects = [];
      if(this.Categories)
      this.Categories.forEach(category => {
        let numberOfServicesSelected = 0;
        let catServices: Services[] = category.services;
        if(catServices)
        catServices.forEach(service => {
          let serviceFound = false;
          appoSersArray.some(function (appoSer, index) {
            if (service.sser_name.toLowerCase() === appoSer.toLowerCase()) {
              __selectedServicesObjects.push(service);
              serviceFound = true;
              numberOfServicesSelected += 1;
              return true;
            } else {
              serviceFound = false;
              return false;
            }
          });

          if (serviceFound) {
            return (service.checked = true);
          } else {
            return (service.checked = false);
          }
        });
        category.numberOfServicesSelected = numberOfServicesSelected;
        return (category.expanded = false);
      });
      this.selectedServicesObjects = __selectedServicesObjects;
    } else {
      if(this.Categories)
      this.Categories.forEach((category, index) => {
        let catServices: Services[] = category.services;
        if(catServices)
        catServices.forEach((service, serviceIndex) => {
          if (index === 0) {
            this.ServiceTapped(index, serviceIndex)
            return (service.checked = true);
          } else {
            return (service.checked = false);
          }
          // index === 0 ? (service.checked = true) : (service.checked = false);
        });
        category.numberOfServicesSelected = 0;

        return (category.expanded = false);
      });
    }

  }

  ServiceTapped(catIndex, serIndex) {

    let selectedCategory = this.Categories[catIndex];

    let servicesArray = selectedCategory.services;
    let selectedService = servicesArray[serIndex];

    selectedService.checked = !selectedService.checked;

    servicesArray[serIndex] = selectedService;
    if (!this.selectedServicesObjects) this.selectedServicesObjects = []
    if (this.selectedServicesObjects && this.selectedServicesObjects.length === 0) {
      selectedCategory.numberOfServicesSelected += 1;
      this.selectedServicesObjects.push(selectedService);
    } else if (selectedService.checked) {
      selectedCategory.numberOfServicesSelected += 1;
      this.selectedServicesObjects.push(selectedService);
    } else if (!selectedService.checked) {
      selectedCategory.numberOfServicesSelected -= 1;
      let serIndex = this.selectedServicesObjects.indexOf(selectedService);
      this.selectedServicesObjects.splice(serIndex, 1);
    }

    this.Categories[catIndex] = selectedCategory;
    this.Categories[catIndex].services = servicesArray;
  }
}
