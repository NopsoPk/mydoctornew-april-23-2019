import {
  NativePageTransitions,
  NativeTransitionOptions
} from "@ionic-native/native-page-transitions";
import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  Platform,
  Navbar
} from "ionic-angular";
import {
  Salon,
  Sal_Techs,
  Services,
  SalonServices,
  Tech_Slots,
  Customer,
  Tech_appointments,
  SALON_CARD_OPTIONS,
  SALON_PROMOTION
} from "../../providers/SalonAppUser-Interface";
import { MenuController } from "ionic-angular";
import { ThanksVcPage } from "../../pages/thanks-vc/thanks-vc";

import "rxjs/add/operator/retrywhen";
import "rxjs/add/operator/delay";
import "rxjs/add/operator/scan";
import { GlobalProvider } from "../../providers/global/global";
import { MainHomePage } from "../main-home/main-home";
import { CustomerModal } from "../../providers/CustomerModal/CustomerModal";
import { config } from "../../providers/config/config";
import { GloballySharedData } from "../../providers/global-service/global-service";
import { AppVersion } from "@ionic-native/app-version";
import { AppMessages } from "../../providers/AppMessages/AppMessages";
import { HomePage } from "../home/home";
import { LoginSignUpPage } from '../login-sign-up/login-sign-up';

@IonicPage()
@Component({
  selector: "page-confirm-booking",
  templateUrl: "confirm-booking.html"
})
export class ConfirmBookingPage {

  allSalons = [];

  @ViewChild(Navbar) navBar: Navbar;
  options: NativeTransitionOptions = {
    direction: "left",
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 0,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0, // not to include this top section in animation
    fixedPixelsBottom: 0 // not to include this Bottom section in animation
  };

  public days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];
  public baseUrl = config.TechImageURL;
  public unregisterBackButtonAction: any;
  public objSubStyle: any;
  public pinImage: any;
  public AppointmentDayName = "";
  public AppointmentDateForConfirmation = "";
  selectedSalon: Salon;
  public salonForComponent: Salon;
  selectedServicesObjects: SalonServices[];
  selectedTech: Sal_Techs;
  selectedSlot: Tech_Slots;
  public appointmentServices = "";
  public appointmentSer_ids = "";
  public appointmentTotalTime = 0;
  public appointmentTotalPrice = 0.0;
  public params: any;
  public myCustomer: Customer;
  public rescheduleAppointment: Tech_appointments;
  public selectedPromotion: SALON_PROMOTION;
  public currencySymbol = "";
  // servicesArray = [];
  // appo_Services: any[]
  // subStyleServicesObject: any[]
  // objAppoServices: any[]
  // objSelectedServices: Services[] = []
  objSelectedServices = [1, 2, 3, 4, 5, 6];
  height = 0;
  public salonCardOption: SALON_CARD_OPTIONS;
  public selctedStoredCredit = 100
  public selectedPromotions = { pc_max_value: 3 }
  ngModelUseCredit: boolean = false
  ngModelUsePromotion: boolean = false

  public objDoctorInfo: any
  // public sal_register_as = 2
  constructor(
    private menu: MenuController,
    public platform: Platform,

    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: GlobalProvider,
    private nativePageTransitions: NativePageTransitions,
    public customerModal: CustomerModal,
    public gsp: GloballySharedData,
    // private admobFree : AdMobFree,
    public loader: LoadingController,
    public appVersion: AppVersion
  ) {
    this.objDoctorInfo = {

    }
    this.currencySymbol = this.gsp.currencySymbol;

    this.selectedSalon = this.navParams.get("selectedSalon");

    console.log('selectedSalonHere', JSON.stringify(this.selectedSalon))

    this.salonForComponent = this.navParams.get("salonForComponent");
    this.selectedServicesObjects = this.navParams.get("selectedServicesObjects");
    this.selectedTech = this.navParams.get("selectedTech");
    
    if(this.selectedTech.tech_discounted_fee === this.selectedTech.tech_fee)
    this.appointmentTotalPrice = Number(this.selectedTech.tech_fee)
    else this.appointmentTotalPrice = Number(this.selectedTech.tech_discounted_fee)

    this.selectedSlot = this.navParams.get("selectedSlot");
    this.rescheduleAppointment = this.navParams.get("rescheduleAppointment");
    this.selectedPromotion = this.navParams.get("selectedPromotion");

    this.serviceManager.setInLocalStorage('this.selectedSalon', this.selectedSalon)
    this.serviceManager.setInLocalStorage('this.salonForComponent', this.salonForComponent)
    this.serviceManager.setInLocalStorage('this.selectedServicesObjects', this.selectedServicesObjects)
    this.serviceManager.setInLocalStorage('this.selectedTech', this.selectedTech)
    this.serviceManager.setInLocalStorage('this.selectedSlot', this.selectedSlot)
    this.serviceManager.setInLocalStorage('this.rescheduleAppointment', this.rescheduleAppointment)
    this.serviceManager.setInLocalStorage('this.selectedPromotion', this.selectedPromotion)

    /**  */
    // this.selectedSalon = this.serviceManager.getFromLocalStorage('this.selectedSalon')
    // this.salonForComponent = this.serviceManager.getFromLocalStorage('this.salonForComponent')
    // this.selectedServicesObjects = this.serviceManager.getFromLocalStorage('this.selectedServicesObjects')
    // this.selectedTech = this.serviceManager.getFromLocalStorage('this.selectedTech')

    // this.selectedSlot = this.serviceManager.getFromLocalStorage('this.selectedSlot')
    // this.rescheduleAppointment = this.serviceManager.getFromLocalStorage('this.rescheduleAppointment')
    // this.selectedPromotion = this.serviceManager.getFromLocalStorage('this.selectedPromotion')

    let Salons: Salon[] = [];
    let sal_rating = Number(this.salonForComponent.sal_rating);
    var _CardHeight = "100pt";
    if (sal_rating > 0) {
      _CardHeight = "80pt";
    }

    console.log('this.selectedTech', JSON.stringify(this.selectedTech))

    // this.sal_register_as = this.salonForComponent.sal_register_as

    // this.salonCardOption = {
    //   CardHeight: _CardHeight,
    //   calledFrom: config.ConfirmBookingPage,
    //   ShouldShowSalonImage: false,
    //   shouldShowQualitifcation: true,
    //   shouldScroll: false,
    //   salon: Salons
    // };

    if (Number(this.selectedSalon.sal_register_as) < 2) { //as a doctor
      this.objDoctorInfo.name = this.selectedSalon.sal_name
      this.objDoctorInfo.specialty = this.selectedSalon.sal_specialty
      this.objDoctorInfo.degree = this.selectedSalon.sal_degree
      this.objDoctorInfo.pic = config.salonImgUrl+this.selectedSalon.sal_pic
      this.objDoctorInfo.location = this.selectedTech.tech_name + ' , ' + this.selectedTech.tech_specialty
    } else { // as a clinic

      this.objDoctorInfo.name = this.selectedTech.tech_name
      this.objDoctorInfo.specialty = this.selectedTech.tech_specialty
      this.objDoctorInfo.degree = this.selectedTech.tech_degree
      this.objDoctorInfo.pic = config.TechImageURL+ this.selectedTech.tech_pic
      this.objDoctorInfo.location = this.selectedSalon.sal_name + ' , ' + this.selectedSalon.sal_address
    }

    // if (this.sal_register_as > 1) {
    //   //in case of clinick

    //   this.salonForComponent.sal_name = this.selectedTech.tech_name
    //   this.salonForComponent.sal_specialty = this.selectedTech.tech_specialty
    //   this.salonForComponent.sal_degree = this.selectedTech.tech_degree
    //   this.salonForComponent.sal_phone = this.selectedTech.tech_pic
    //   this.allSalons.push(this.salonForComponent);
    //   Salons.push(this.salonForComponent);
    //   console.log('salonForComponent', JSON.stringify(this.salonForComponent))

    // } else {
    //   //in case of doctor
    //   this.salonForComponent.sal_address = this.selectedTech.tech_specialty
    //   this.allSalons.push(this.salonForComponent);
    //   Salons.push(this.salonForComponent);
    // }
    this.params = {};

    // let response = this.serviceManager.getFromLocalStorage('_salonDetail')
    // this.selectedSalon = response["salons"][0];

    // this.params = this.navParams.get("params");
    // this.servicesArray = atob(this.params["app_services"]).split(',');
    // this.height = this.servicesArray.length * 40;
    // this.selectedSalon = this.navParams.get("selectedSalon");
    // this.selectedTech = this.navParams.get("selectedTech");
    // this.appo_Services = this.navParams.get("appo_Services");
    // this.subStyleServicesObject = []
    // this.objAppoServices = []
    // this.appo_Services.forEach(ser => {
    //   let service: Services = ser

    //   if (service.ssubc_id) {
    //     this.subStyleServicesObject.push(service)
    //   } else {
    //     this.objAppoServices.push(service)
    //   }
    // });
  }

  //custom back button
  ionViewWillEnter() {
    this.getCustomer();
  }
  ionViewDidEnter() {
    this.getAppointmentCharges()
    this.initializeBackButtonCustomHandler();
    this.getAppVersion();
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      () => {
        this.customHandleBackButton();
      },
      10
    );
  }
  private customHandleBackButton(): void {
    if (this.menu.isOpen()) {
      this.menu.close();
    } else {
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop();
      } else {
        this.navCtrl.setRoot(HomePage);
      }
    }

    // this.navCtrl.pop();
  }
  //end custom back button

  ionViewDidLoad() {
    // this.selectedServicesObjects.forEach(objService => {
    //   this.appointmentServices += objService.sser_name + ",";
    //   this.appointmentSer_ids += objService.sser_id + ",";
      // this.appointmentTotalTime += Number(objService.sser_time);
    //   this.appointmentTotalPrice += Number(objService.sser_rate);

    // });

    this.appointmentServices = this.appointmentServices.slice(
      0,
      this.appointmentServices.length - 1
    );
    this.appointmentSer_ids = this.appointmentSer_ids.slice(
      0,
      this.appointmentSer_ids.length - 1
    );

    var dateObject = new Date(this.selectedSlot.ts_date);
    this.AppointmentDayName = this.days[dateObject.getDay()];
    this.AppointmentDateForConfirmation = this.serviceManager.getDateFormattedForAppointmentConfirmation(
      this.selectedSlot.ts_date
    );
  }

  getBookingDate() {
    let tempDate = atob(this.params["app_start_time"]);
    let splitted = new Date(tempDate.split(" ")[0]);
    return this.serviceManager.getStringFromDateWithFormat(splitted, "EEE dd, MMM");
  }

  getBookingTime() {
    let tempDate = atob(this.params["app_start_time"]);
    let splitted = tempDate.split(" ")[1];
    return this.serviceManager.get12HourFormatFrom24Hour(splitted);
  }
  getTotalPrice() {
    return atob(this.params["app_price"]);
  }
  getTotalTime() {
    return atob(this.params["app_est_duration"]);
  }
  btnBookAppointmentTapped() {
    if (!this.myCustomer) {

      this.nativePageTransitions.slide(this.options);
      this.navCtrl.push(LoginSignUpPage, {
        comingFrom: "SalonDetailsPage"
      });

      return;
    }
    let start_time =
      this.serviceManager.getStringFromDateWithFormat(
        this.selectedSlot.ts_date,
        "yyyy-MM-dd"
      ) +
      " " +
      this.selectedSlot.ts_start_time +
      ":00";
    // alert(this.selectedSalon.sal_appointment_interval)
    this.params = {
      service: btoa("make_appointment"),
      app_type: btoa("appointment"), // this string is as it is. not know whats benefit of it
      sal_id: btoa(this.selectedSalon.sal_id),
      cust_id: btoa(this.myCustomer.cust_id.toString()),
      tech_id: btoa(this.selectedTech.tech_id.toString()),
      app_id: btoa(""),
      app_services: btoa(this.appointmentServices),
      app_service_ids: btoa(this.appointmentSer_ids),
      app_price: btoa(this.appointmentTotalPrice.toString()),
      app_est_duration: btoa(this.selectedSalon.sal_appointment_interval),//this.appointmentTotalTime.toString()
      app_start_time: btoa(start_time),
      app_slots: btoa(this.selectedSlot.ts_number),
      app_user_info: btoa(this.appInfo), //version info build,api,version etc
      device_datetime: btoa(this.serviceManager.getCurrentDeviceDateTime())
    };
    if (this.selectedSalon.sal_auto_accept_app == 0) {
      this.params["app_status"] = btoa(this.serviceManager.APP_STATUS_PENDING);
    } else if (this.selectedSalon.sal_auto_accept_app == 1) {
      this.params["app_status"] = btoa(this.serviceManager.APP_STATUS_ACCEPTED);
    }
    if (this.selectedPromotion) {
      this.params["so_id"] = btoa(this.selectedPromotion.so_id.toString());
    }
    if (this.rescheduleAppointment) {
      this.params["service"] = btoa("update_appointment");
      this.params["app_id"] = btoa(this.rescheduleAppointment.app_id);
      console.log("Updating...");
    }

    let loading = this.loader.create({
      content: "Booking Appointment, Please Wait..."
    });
    loading.present();
    this.serviceManager
      .getData(this.params)
      .retryWhen(err => {
        return err
          .scan(retryCount => {
            retryCount += 1;
            if (retryCount < 3) {
              return retryCount;
            } else {
              throw err;
            }
          }, 0)
          .delay(1000);
      })

      .subscribe(
        res => {
          console.log(JSON.stringify(res));

          loading.dismissAll();
          if (Number(res.status) === 1) {
            if (Number(this.selectedSalon.sal_temp_enabled) === 1) {
              this.serviceManager.makeToastOnSuccess(
                AppMessages.msgOnRequestAppointment
              );
            }
            this.nativePageTransitions.slide(this.options);
            (this.gsp.sty_id = this.salonForComponent.sty_id),
              this.navCtrl
                .push(ThanksVcPage, {
                  sal_name: this.salonForComponent.sal_name,
                  sal_typeID: this.salonForComponent.sty_id
                })
                .then(done => {
                  if (this.selectedSalon.sal_temp_enabled === "1") {
                    this.serviceManager.makeToastOnSuccess(
                      AppMessages.msgOnRequestAppointment
                    );
                  }
                });
          } else {
            this.serviceManager.makeToastOnFailure(res.msg);
          }
        },
        error => {
          loading.dismissAll();
          this.serviceManager.makeToastOnFailure(
            AppMessages.msgNoInternetAVailable
          );
          console.log("something went wrong", error);
        }
      );
    console.log(this.params);
  }

  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: "right",
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };

    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options);

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(HomePage);
    }
  }
  getSlotFromattedTime(slotTime) {
    return this.serviceManager.get12HourFormatFrom24Hour(slotTime);
  }
  getCustomer() {
    this.customerModal.getDatabaseState().subscribe(ready => {
      if (ready) {
        this.customerModal.getCustomer().then(
          customer => {
            this.myCustomer = customer;
          },
          error => { }
        );
      }
    });
  }

  appInfo = "";
  getAppVersion() {
    this.appVersion.getAppName().then(appName => {
      this.appInfo += "app_Name: " + appName + ",\n";
      this.appVersion.getPackageName().then(packageName => {
        this.appInfo += "package_Name: " + packageName + ",\n";
        this.appVersion.getVersionCode().then(versionCode => {
          this.appInfo += "version_Code: " + versionCode + ",\n";
          this.appVersion.getVersionNumber().then(versionNumber => {
            this.appInfo += "versionNumber: " + versionNumber + ",\n";
          });
        });
      });
    });
  }
  roundAmount(amount: number | string) {
    return Math.round(Number(amount));
  }
  showStoredCreditDetail() {

  }
  ionToggleChanged(event) {

    // if (this.ngModelUseCredit) {
    //   this.appointmentTotalPrice = this.appointmentTotalPrice - this.selctedStoredCredit
    // } else {
    //   this.appointmentTotalPrice = this.appointmentTotalPrice + this.selctedStoredCredit
    // }
  }
  // ionTogglePromotionChanged(event) {

  //   if (this.ngModelUsePromotion) {
  //     this.appointmentTotalPrice = this.appointmentTotalPrice - (this.appointmentTotalPrice * 0.03)
  //   } else {
  //     this.appointmentTotalPrice = this.appointmentTotalPrice + (this.appointmentTotalPrice * 0.03)

  //   }
  //   this.appointmentTotalPrice = Number(Number(this.appointmentTotalPrice).toFixed(0))
  // }
  appointmentCharges: number = 0
  appointmentDiscount = 0
  gotBookCharges
  getAppointmentCharges() {

    const params = {
      service: btoa('get_booking_charges'),
      tech_id: btoa(this.selectedTech.tech_id.toString()),
    }

    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          this.gotBookCharges = true
          this.appointmentCharges = res.app_fee
          // this.appointmentCharges = 225
          if (this.appointmentCharges === 0) {
            return
          }

          this.appointmentDiscount = this.appointmentCharges * (Number(res.app_fee_discount) / 100)
          this.appointmentTotalPrice += Number(this.appointmentCharges - this.appointmentDiscount)
          // this.appointmentTotalPrice -= Number(this.appointmentDiscount)

        },
        (error) => {
          console.log(error);
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

        },
        () => {
          this.gotBookCharges = true
          // this.serviceManager.stopProgress()
        }
      )

  }
}
