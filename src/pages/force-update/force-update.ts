import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { AppVersion } from '@ionic-native/app-version';
import { AppMessages } from '../../providers/AppMessages/AppMessages';
import { MainHomePage } from '../main-home/main-home';
import { Platform } from 'ionic-angular/platform/platform';

/**
 * Generated class for the ForceUpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-force-update',
  templateUrl: 'force-update.html',
})
export class ForceUpdatePage {
  public forceUpdateMessage = ''
  public appInfo = ''
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: GlobalProvider,
    public appVersion: AppVersion,
    public appCtrl: App,
    public platForm: Platform,
  ) {
    this.forceUpdateMessage = navParams.get('update_msg')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForceUpdatePage');
    this.platForm.ready().then(() => {
      this.platForm.is('android') ? this.appPlatform = 'android' : this.appPlatform = 'ios'

      if (!this.forceUpdateMessage) this.getAppVersionAndcheckForceUpdate()

    })

  }

  getAppVersionAndcheckForceUpdate() {

    this.appInfo = ''
    try {
      this.appVersion.getAppName().then(appName => {
        this.appName = appName
        this.appInfo += 'app_Name: ' + appName + ',\n'
        this.appVersion.getPackageName().then(packageName => {
          this.appInfo += ('package_Name: ' + packageName + ',\n')
          this.appVersion.getVersionCode().then(buildNo => {
            this.buildNo = buildNo
            this.appInfo += ('version Code: ' + buildNo + ',\n')
            this.appVersion.getVersionNumber().then(versionNumber => {
              this.Version = versionNumber
              this.appInfo += ('versionNumber: ' + versionNumber + ',\n')
              this.checkForceUpdate()

            })
          })
        })
      })
    } catch (error) {

    }

  }
  public appPlatform = ''
  public Version = ''
  public buildNo = ''
  public appName = ''
  public showForceUpdatePushed = false;
  checkForceUpdate() {

    this.buildNo = '5'
    const params = {
      service: btoa('force_update'),
      platform: btoa(this.appPlatform),
      app_version: btoa(this.Version),
      app_build_no: btoa(this.buildNo),
      app_name: btoa(this.appName)
    }

    this.serviceManager.showProgress('Please Wait....')
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(
        (res) => {
          this.serviceManager.stopProgress()
          if (res.force_update && Number(res.force_update) === 1) {
            this.serviceManager.setInLocalStorage(this.serviceManager.IS_FORCE_UPDATE_AVAILABLE, true)
            this.serviceManager.setInLocalStorage(this.serviceManager.FORCE_UPDATE_MESSAGE, res.update_msg)
            this.serviceManager.setInLocalStorage('lastForceUpdateFetchTime', res.response_datetime)
            this.forceUpdateMessage = res.update_msg

          } else {

            this.serviceManager.setInLocalStorage('lastForceUpdateFetchTime', res.response_datetime)

            this.serviceManager.removeFromStorageForTheKey(this.serviceManager.IS_FORCE_UPDATE_AVAILABLE)
            this.appCtrl.getRootNav().setRoot(MainHomePage, {

            })

          }
        },
        (error) => {
          this.serviceManager.stopProgress()
          this.serviceManager.makeToastOnFailure(AppMessages.msgNoInternetAVailable)

          console.log('something went wrong', error);
        }
      )
  }
}
