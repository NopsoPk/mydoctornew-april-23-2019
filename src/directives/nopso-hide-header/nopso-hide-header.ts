import { Directive, Input, ElementRef, Renderer2, Renderer } from '@angular/core';
import { Content, Events } from 'ionic-angular';
import { NOPSO_Header_OPTIONS } from '../../providers/SalonAppUser-Interface';


@Directive({
  selector: '[nopso-hide-header]' // Attribute selector
})
export class NopsoHideHeaderDirective {
  @Input("nopso-hide-header") content: Content;
  @Input() NopsoHeaderOptions: NOPSO_Header_OPTIONS;

  header: HTMLElement;
  headerHeight: number;
  lastScrollTop: number = 0;
  translateAmt: number = 0;

  //old header variables
  opacity: number = 0;
  newHeaderHeight: any;
  headerHeight2: number;
  headerScrolHeight = 150;
  heightIsStillZero = false
  isEventPublished = false
  isFalseEVentPublished = false

  showHeaderEventPublishAlready = false
  hideHeaderEventPublishAlready = false

  //new try
  public newScrollValue = 0

  constructor(
    public element: ElementRef,
    public renderer2: Renderer2,
    public renderer: Renderer,
    public events: Events,
  ) {
    console.log('Hello NopsoHideHeaderDirective Directive');
  }
  ngOnInit() {

    this.header = this.element.nativeElement;

    this.content.ionScroll.subscribe(ev =>


      requestAnimationFrame(() => this.updateElasticHeader(ev.scrollTop, ev))
    );
  }
  updateElasticHeader(scrollTop: number, ev) {
    // return ev.scrollTop
    // console.log(ev);
    if (ev.directionY === 'down') {
      // console.log('scrolling to down ' + scrollTop);

    } else if (ev.directionY === 'up') {
      // console.log('scrolling to up'+ scrollTop);
    }

    !this.headerHeight && (this.headerHeight = this.header.clientHeight);

    if (this.lastScrollTop < 0) this.translateAmt = 0;
    else {

      this.opacity = (1 - (scrollTop / 300))

      if (this.opacity < 0.5) {
        this.opacity = 0.5
      }

      this.translateAmt += (this.lastScrollTop - scrollTop) / 4;
      if (this.translateAmt > 0) this.translateAmt = 0;
      if (this.translateAmt < -this.headerHeight - 12)
        this.translateAmt = -this.headerHeight - 12;
    }

    !this.headerHeight2 && (this.headerHeight2 = this.header.clientHeight);

    this.newHeaderHeight = (this.headerHeight2 - (scrollTop / 4));
    this.renderer2.setStyle(
      this.header,
      "height",
      this.newHeaderHeight + "px",
    );
    // console.log('this.newHeaderHeight '+this.newHeaderHeight);

    for (let headerElement of this.element.nativeElement.children) {

      let totalHeight = headerElement.offsetTop + headerElement.clientHeight;

      if (totalHeight > this.newHeaderHeight && !headerElement.isHidden) {

        headerElement.isHidden = true;

        this.renderer.setElementStyle(headerElement, 'opacity', '0');
        this.renderer2.setStyle(headerElement, 'visibility', 'hidden');


      } else if (totalHeight <= this.newHeaderHeight && headerElement.isHidden) {
        headerElement.isHidden = false;
        this.renderer.setElementStyle(headerElement, 'opacity', '1');
        this.renderer2.setStyle(headerElement, 'visibility', 'visible');
      }

    }
    this.lastScrollTop = scrollTop;
    if (this.newHeaderHeight < 70) {
      if (this.showHeaderEventPublishAlready) return

      this.events.publish('canShowMiniHeader', true);
      this.showHeaderEventPublishAlready = true
      this.hideHeaderEventPublishAlready = false

    } else if (this.newHeaderHeight >= 70) {

      if (this.hideHeaderEventPublishAlready) return

      this.events.publish('canShowMiniHeader', false);
      this.hideHeaderEventPublishAlready = true
      this.showHeaderEventPublishAlready = false


    }

    /*
      // event firing when to show or hide mini header
      if (this.newHeaderHeight < 70) {
        this.newHeaderHeight = 0;
        this.heightIsStillZero = true
        this.isFalseEVentPublished = false
      } else {
        this.heightIsStillZero = false
        this.isEventPublished = false
        if (!this.isFalseEVentPublished) {
          this.events.publish('canShowMiniHeader', false);
          this.isFalseEVentPublished = true
        }
  
      }
      if (this.heightIsStillZero === true && this.isEventPublished === false) {
  
        this.events.publish('canShowMiniHeader', true);
        this.isEventPublished = true
      }
      */
  }
}
