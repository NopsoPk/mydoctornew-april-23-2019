export interface PLACE_ORDER_PARAM{
  service?: string,
  cust_id?: string,
  o_address?: string,
  o_city?: string,
  o_province?: string,
  o_amount?: string,
  o_delivery_charges?: string,
  o_contact_person? :string,
  o_phone?: string,
  pt_id?: string,
  o_payment_mobile? : string,
  o_items?: ORDER_ITEM[]
  o_payable_amount?: string
  pc_id? : string,
  o_cust_store_credit? : string,
  o_discount? : string,
  o_cod_charges? : string,
  pc_id_public? : string,
  o_discount_public? : string,
}

export interface ORDER_ITEM {
  p_id?: string,
  od_price?: string,
  od_quantity?: string,
  od_name?: string,
  od_image?: string,
  
}