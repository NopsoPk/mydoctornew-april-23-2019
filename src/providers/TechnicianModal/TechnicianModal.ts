import { Sal_Techs, TECH_HOURS } from '../SalonAppUser-Interface';
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from '../global/global';
import { BehaviorSubject } from "rxjs/Rx";
import { Storage } from "@ionic/storage";

@Injectable()
export class TechnicianModal {

  /* Global Vairables */
  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;

  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider, ) {
    console.log('Hello Technician Modal ');

    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('database is setup');
          this.databaseReady.next(true)
        }, err => {
          console.log('database is nullll');
        })
    });
  }
  getDatabaseState() {
    return this.databaseReady.asObservable()
  }
  createTechnicianTable() {
    let createTableQuery = "CREATE TABLE IF NOT EXISTS `tech_hours` (`tech_id` int(6),`sal_id` int(4),`tech_hours` varchar(64) ,`tech_day` varchar(64) )"

    let isTableCreated = false
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        isTableCreated = true
        console.log('tech_hours table created');
        return isTableCreated
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }
        console.log('error: Technicians' + e.message);
        let errorMessage: string = e.message
        if (errorMessage.trim().toLowerCase() === 'table tech_hours already exists'.trim().toLowerCase()) {
          isTableCreated = true
          return isTableCreated
        } else {
          isTableCreated = false
          return isTableCreated
        }
      });
  }
  InsertInToTechHoursTable(myTechs: TECH_HOURS[]) {
    // alert('insertion test myTechs leng'+myTechs.length)
    if (!myTechs) return

    let basePart = "INSERT OR REPLACE INTO `tech_hours` (`tech_id`, `sal_id`, `tech_hours`, `tech_day`) VALUES"
    let valuesPart = ''
    let techHoursID = ''
    myTechs.forEach(tech => {
      valuesPart += '("' + tech.tech_id + '", "' + tech.sal_id + '", "' + tech.tech_hours + '", "' + tech.tech_day + '"),';
      techHoursID += tech.tech_id+','
    });
    valuesPart = valuesPart.slice(0, valuesPart.length - 1)
    techHoursID = techHoursID.slice(0, techHoursID.length - 1)
    let finalQuery = basePart + valuesPart
    console.log(finalQuery);
    
    return this.TruncCateTechnicianTable(techHoursID).then(isDeleted => {
      let isDataInserted = false
      return this.database.executeSql(finalQuery, []).then(res => {
        console.log('Technician insert '+res.rowsAffected);
        // alert('Technician insert '+res.rowsAffected)
        console.log(res);

        res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
        return isDataInserted
      }).catch(e => {
        // alert(e.message)
        this.serviceManager.sendErrorToServer(e.message, finalQuery)
        isDataInserted = false
        return e.message

      });
    })

    // return this.TruncCateTechnicianTable().then(tblHomeDeleted => {
    //   let isDataInserted = false

    // })
  }

  getTechHoursFromDB(techID) {
    let SalonHours: TECH_HOURS[] = []
    let Query = 'SELECT * FROM `tech_hours` where tech_id =  "' + techID + '" ';
    return this.database.executeSql(Query, [])
      .then(res => {
        // alert(res.rows.length)
        for (var i = 0; i < res.rows.length; i++) {
          let hours: TECH_HOURS = res.rows.item(i)
          SalonHours.push(hours)
        }
        return SalonHours
      })
      .catch(e => {
        // alert(e.message)
        return SalonHours = []
      });
  }

  //  
  TruncCateTechnicianTable(techIDS) {
    let deleteQuery = "DELETE FROM `tech_hours` where `tech_id` IN (" + techIDS + ");"
    let isDeleted = false
    return this.database.executeSql(deleteQuery, [])
      .then(res => {
        console.log('success: app_home_page_section deleted');
        isDeleted = true
        console.log('deleted');

        return isDeleted
      })
      .catch(e => {
        isDeleted = false
        this.serviceManager.sendErrorToServer(e.message, deleteQuery)
        console.log('error: test deletion ' + e.message);

        return e.message

      });
  }
  dropTable() {

    let deleteSelectedQuery = 'DELETE FROM search;'
    return this.database.executeSql(deleteSelectedQuery, [])
      .then(res => {
        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery)
        return false;
      });
  }
  updateTechnicianTable(lat, lng) {
    let updateQuery = "UPDATE `tech_hours` SET `cust_lat` =  " + lat + ", `cust_lng` = " + lng
    return this.database.executeSql(updateQuery, [])
      .then(res => {
        console.log('success: Technician updated');
        return true
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, updateQuery)
        console.log('error: ' + e.message);
        return false
      });
  }
  async updateTechnicianStoreCredit(cust_creditslat) {
    let updateQuery = "UPDATE `tech_hours` SET `cust_store_credit` =  " + cust_creditslat
    try {
      const res = await this.database.executeSql(updateQuery, []);
      console.log('success: Technician updated');
      return true;
    }
    catch (e) {

      this.serviceManager.sendErrorToServer(e.message, updateQuery);
      console.log('error: ' + e.message);
      return false;
    }
  }

}
