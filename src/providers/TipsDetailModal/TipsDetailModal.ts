import {TIPS_DETAIL } from '../SalonAppUser-Interface';
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from '../global/global';
import { BehaviorSubject } from "rxjs";
import { Storage } from "@ionic/storage";


@Injectable()
export class TipsDetailModal {

  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;


  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider, ) {
    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('database is setup');
          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }

  getDatabaseState() {
    return this.databaseReady.asObservable()
  }

  createTipsDetailTable() {
    console.log('came in beauty_tips_detail Modal');

    

    let createTableQuery = "CREATE TABLE IF NOT EXISTS `beauty_tips_detail` (`bt_is_featured` tinyint(1), `bt_id` int(11), `btc_id` int(11) ,`bt_title` varchar(200) ,`bt_short_description` text ,`bt_datetime` varchar(250) ,`ssc_id` tinyint(11) ,`bt_status` tinyint(11) ,`bt_URL` varchar(250) DEFAULT NULL,`bt_description` text ,`bt_imageUrl` varchar(250) ,`bt_modified_date` timestamp, `bt_title_urdu` varchar(200) ,`bt_description_urdu` text,`bt_short_description_urdu` text) "
    let isTableCreated = false
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        isTableCreated = true
        console.log('beauty_tips_detail table created');
        
        return res
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }


        console.log('error: InsertInToBeautyTipsTable ' + e.message);
        let errorMessage: string = e.message
        if (errorMessage.trim().toLowerCase() === 'table beauty_tips_detail already exists'.trim().toLowerCase()) {
          isTableCreated = true
          return isTableCreated
        } else {
          isTableCreated = false
          return isTableCreated
        }
      });
  }

  InsertInToTipsDetailTable(tipsDetailData: TIPS_DETAIL[]) {
    if (!tipsDetailData || tipsDetailData.length === 0) {
      return
    }
    let re = /\'/gi;

    let firstPart = "INSERT INTO `beauty_tips_detail` (`bt_is_featured`,  `bt_id`, `btc_id` , `bt_title`, `bt_short_description`, `bt_datetime`, `ssc_id`, `bt_status`, `bt_URL`, `bt_description`, `bt_imageUrl`, `bt_modified_date`, `bt_title_urdu`, `bt_description_urdu`, `bt_short_description_urdu`) VALUES ";
    let secondPart = ""
    let beautyTipsIds = ""

    //console.log('insertionTest: ' + tipsDetailData.length + ' beauty_tips_detail were sent for dumping');
    
    tipsDetailData.forEach(beautyTip => {
        let re = /\'/gi;
        secondPart += '("' + beautyTip.bt_is_featured + '","'+ beautyTip.bt_id + '", "'+ beautyTip.btc_id + '", "' + beautyTip.bt_title.replace("\"","'")  + '", \'' + beautyTip.bt_short_description_english.replace(re,"`") + '\', "' + beautyTip.bt_datetime + '" , "' + beautyTip.ssc_id + '", "' + beautyTip.bt_status + '", "' + beautyTip.bt_URL + '", \'' + beautyTip.bt_description.replace(re,"`") + '\', "' + beautyTip.bt_imageUrl + '", "' + beautyTip.bt_modified_date + '", \'' + beautyTip.bt_title_urdu.replace(re,"`")  + '\', \'' + beautyTip.bt_description_urdu.replace(re,"`") + '\', \'' + beautyTip.bt_short_description_urdu.replace(re,"`") + '\'),';
        beautyTipsIds += beautyTip.bt_id +','
    //temp code
    // let finalQuery = firstPart + secondPart
    // let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    // secondPart=''//temp
    // beautyTipsIds = beautyTipsIds.slice(0, beautyTipsIds.length - 1)
    // return this.delteDuplicateBeautyTips(beautyTipsIds).then(() => {
    //   let isDataInserted = false
    //   return this.database.executeSql(slicedQuery, [])
    //     .then(res => {
    //      // console.log('insertionTest: ' + res.rowsAffected + ' beauty_tips_detail were dumped');
    //       res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
    //       secondPart=''//temp
    //       return isDataInserted
    //     })
    //     .catch(e => {
    //       secondPart=''//temp
    //       console.log('12E0:', e.message+'Query'+slicedQuery);
    //       this.serviceManager.sendErrorToServer(e.message, slicedQuery)
    //       isDataInserted = false
    //       return e.message
    //     });
    // }).catch(e => {  
    //   secondPart=''//temp 
    //   console.log('12E1:', e.message+'Query'+slicedQuery);
    //   this.serviceManager.sendErrorToServer(e.message, slicedQuery)
    //   return e.message
    // });
    //end temp code
      
      //  secondPart='';//for now
    });
    // secondPart=''
    let finalQuery = firstPart + secondPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    beautyTipsIds = beautyTipsIds.slice(0, beautyTipsIds.length - 1)
    return this.delteDuplicateBeautyTips(beautyTipsIds).then(() => {
      let isDataInserted = false
      return this.database.executeSql(slicedQuery, [])
        .then(res => {
         // console.log('insertionTest: ' + res.rowsAffected + ' beauty_tips_detail were dumped');
          res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
          return isDataInserted
        })
        .catch(e => {
          console.log('114:', e.message);
          this.serviceManager.sendErrorToServer(e.message, slicedQuery)
          isDataInserted = false
          return e.message
        });
    }).catch(e => {   
      console.log('123:', e.message);
      this.serviceManager.sendErrorToServer(e.message, slicedQuery)
      return e.message
    });

    
  }
 deleteBeautyTips() {
    let Query = 'DELETE FROM beauty_tips_detail'
    return this.database.executeSql(Query, [])
      .then(res => {
        return true
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Query)
        console.log('error: ' + e.message);
        return false
      });

  }

  getBeautyTipsDetail(btc_id) {

    let Inserquery = 'select * from `beauty_tips_detail` where  btc_id = '+btc_id//(' + btc_id + ');'
    let beautyTips = []
    
    
    return this.database.executeSql(Inserquery, [])
      .then(tipsDetailDataSource => {
        for (var i = 0; i < tipsDetailDataSource.rows.length; i++) {
          beautyTips.push(tipsDetailDataSource.rows.item(i))
        }
        return beautyTips
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Inserquery)
        console.log('error:  tipsDetailDataSource ' + e.message);

        return beautyTips

      });
  }
  getLatestHeroArticle(btc_id) {

    //select bt.*, btc.btc_name from `beauty_tips_detail` as bt, `beauty_tip_categories` as btc where bt.btc_id = btc.btc_id AND ( bt.btc_id = '+btc_id+' or bt.bt_id = '+bt_id+')  limit 2

    
    //Inserquery = 'select bt.*, btc.btc_name from `beauty_tips_detail` as bt, `beauty_tip_categories` as btc where bt.btc_id = btc.btc_id and bt.btc_id = '+btc_id+' order by bt_is_featured desc, bt_modified_date desc limit 0, 100 '
    let Inserquery =null;
    
    if(btc_id){
      //
       Inserquery = 'select bt.*, btc.btc_name  from `beauty_tips_detail` as bt, `beauty_tip_categories` as btc where bt.btc_id= btc.btc_id and bt.btc_id= '+btc_id+' order by bt.bt_is_featured desc, bt.bt_modified_date desc  limit 1'

    }else{
      Inserquery = 'select bt.*, btc.btc_name  from `beauty_tips_detail` as bt, `beauty_tip_categories` as btc where bt.btc_id= btc.btc_id order by bt.bt_is_featured desc, bt.bt_modified_date desc  limit 1'//(' + btc_id + ');'
    
    }
   
    let beautyTip: TIPS_DETAIL
    
    
    return this.database.executeSql(Inserquery, [])
      .then(tipsDetailDataSource => {
        if (tipsDetailDataSource.rows.length > 0) {
          beautyTip =  tipsDetailDataSource.rows.item(0)
        }
        
        return beautyTip
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Inserquery)
        console.log('error:  tipsDetailDataSource ' + e.message);
        
        return beautyTip

      });
  }
  getTwoBeautyTips(btc_id, bt_id) { 
    let Inserquery = 'select bt.*, btc.btc_name from `beauty_tips_detail` as bt, `beauty_tip_categories` as btc where bt.btc_id = btc.btc_id AND bt.btc_id ='+  btc_id + ' AND bt.bt_id !='+bt_id+' order by bt_modified_date desc limit 2 '
    //  let Inserquery = 'select bt.*, btc.btc_name from `beauty_tips_detail` as bt, `beauty_tip_categories` as btc where bt.btc_id = btc.btc_id AND ( bt.btc_id = '+btc_id+' or bt.bt_id = '+bt_id+')  AND bt.bt_id <> '+bt_id+' limit 3'
    let beautyTips = []
    return this.database.executeSql(Inserquery, [])
      .then(tipsDetailDataSource => {
        for (var i = 0; i < tipsDetailDataSource.rows.length; i++) {
          beautyTips.push(tipsDetailDataSource.rows.item(i))
        }
        return beautyTips
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Inserquery)
        console.log('error:  tipsDetailDataSource ' + e.message);
        return beautyTips
      });
  }
  getBeautyTipsNEW(btc_id) {
    //if id is null
   // if(btc_id !)
   let Inserquery=null;
   if(btc_id){
     
     Inserquery = 'select bt.*, btc.btc_name from `beauty_tips_detail` as bt, `beauty_tip_categories` as btc where bt.btc_id = btc.btc_id and bt.btc_id = '+btc_id+' order by bt.bt_is_featured desc, bt.bt_modified_date desc  '//(' + btc_id + ');'
 
     }else{
      Inserquery = 'select bt.*, btc.btc_name from `beauty_tips_detail` as bt, `beauty_tip_categories` as btc where bt.btc_id = btc.btc_id order by bt.bt_is_featured desc, bt.bt_modified_date  desc  '//(' + btc_id + ');'
      }
        //new Id
   // 
    let beautyTips = []
    return this.database.executeSql(Inserquery, [])
      .then(tipsDetailDataSource => {
      
        for (var i = 0; i < tipsDetailDataSource.rows.length; i++) {
          beautyTips.push(tipsDetailDataSource.rows.item(i))
          
            console.log('bt_is_featured',tipsDetailDataSource.rows.item(i).bt_is_featured)
            console.log('bt_title_urdu',tipsDetailDataSource.rows.item(i).bt_title)
          
          

        }
     
        return beautyTips
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Inserquery)
        console.log('error:  tipsDetailDataSource ' + e.message);

        return beautyTips

      });
  }

  TruncateBeautyTipsTable() {
    let deleteQuery = "DELETE FROM `beauty_tips_detail` ;"
    let isDeleted = false

    return this.database.executeSql(deleteQuery, [])
      .then(res => {
        

        isDeleted = true
        return isDeleted
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteQuery)
        console.log('error: beauty_tips_detail');

        isDeleted = false
        return e.message

      });
  }

  delteDuplicateBeautyTips(beautyTipsIds) {

    if (!beautyTipsIds) {
      return 
    }
    let deleteSelectedQuery = 'DELETE FROM `beauty_tips_detail`  where bt_id IN (' + beautyTipsIds + ');'
    return this.database.executeSql(deleteSelectedQuery, [])
      .then(res => {
        return true;
      })
      .catch(e => {
        
        this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery)
        return false;
      });
  }
  
}
