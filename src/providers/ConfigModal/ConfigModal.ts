
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

import { GlobalProvider } from '../global/global';
import { BehaviorSubject } from "rxjs";
import { Storage } from "@ionic/storage";
import { APP_CONFIG } from '../SalonAppUser-Interface';



@Injectable()
export class ConfigModal {
  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;

  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider, ) {
    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('database is setup');
          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }

  getDatabaseState() {
    return this.databaseReady.asObservable()
  }

  createAppConfigTable() {

    let createTableQuery = "CREATE TABLE IF NOT EXISTS `appConfig` ( `app_path` varchar(500),`client_api_path` varchar(500) ,`admin_api_path` varchar(500) , `cust_initial_credit` varchar(255), `environment` varchar(255), `scraping_interval` varchar(255), `last_fetch_id` varchar(255), `last_exported_pin` varchar(255), `search_radius` varchar(255), `currency` varchar(255), `distance_unit` varchar(255) );"
    let isTableCreated = false
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        isTableCreated = true
        console.log('config table created');
        
        return res
      })
      .catch(e => {

        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }


        console.log('error: InsertInToAppConfigTable' + e.message);
        let errorMessage: string = e.message
        if (errorMessage.trim().toLowerCase() === 'table config already exists'.trim().toLowerCase()) {
          isTableCreated = true
          return isTableCreated
        } else {
          isTableCreated = false
          return isTableCreated
        }



      });

  }

  InsertInAppConfigTable(config: APP_CONFIG) {
    if(!config) return 

    let firstPart = "INSERT INTO `appConfig` (`app_path`, `client_api_path`, `admin_api_path`, `cust_initial_credit`, `environment`, `scraping_interval`, `last_fetch_id`, `last_exported_pin`, `search_radius`, `currency`,`distance_unit`) VALUES ";
    let secondPart = ''
    secondPart = '("' + config.app_path + '", "' + config.client_api_path + '", "' + config.admin_api_path + '", "' + config.cust_initial_credit + '", "' + config.environment + '", "' + config.scraping_interval + '", "' + config.last_fetch_id + '", "' + config.last_exported_pin + '", "' + config.search_radius + '", "' + config.currency + '", "' + config.distance_unit + '"  )';
    let insertQuery = firstPart + secondPart
    


    let isDataInserted = false

    return this.database.executeSql("DELETE FROM `appConfig`", [])
      .then(res => {
        return this.database.executeSql(insertQuery, [])
          .then(res => {
            res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
            return isDataInserted
          })
          .catch(e => {
            this.serviceManager.sendErrorToServer(e.message, insertQuery)
            isDataInserted = false
            console.log('error: ' + e.message);
            return e.message
          });
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, 'DELETE FROM `config`')
        console.log('error: ' + e.message);
      });




  }

  getAllAppConfig() {
    let selectQuery = "select * from `appConfig`"
    let appConfigData: APP_CONFIG
    return this.database.executeSql(selectQuery, []).then(configDataSet => {
      appConfigData = configDataSet.rows.item(0)
      return appConfigData

    })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, selectQuery)
        console.log('error:  appConfig ' + e.message);
        return appConfigData

      });
  }
  getCurrencyConfig(currency: string) {
    let selectQuery = "select * from `appConfig` where config_name =" + currency

    return this.database.executeSql(selectQuery, []).then(configDataSet => {

      let appConfigData: APP_CONFIG = configDataSet.rows.item(0)
      appConfigData ? appConfigData : false
    })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, selectQuery)
        console.log('error:  appConfig ' + e.message);
        return false

      });
  }
  //  
  truncCateAppConfigTable() {
    let isDeleted = false
    
    return this.database.executeSql("DELETE FROM `appConfig`", [])
      .then(res => {
        

        isDeleted = true
        return isDeleted
      })
      .catch(e => {

        this.serviceManager.sendErrorToServer(e.message, 'DELETE FROM `appConfig`')
        
        console.log('error: config');

        isDeleted = false
        return e.message

      });
  }

}
