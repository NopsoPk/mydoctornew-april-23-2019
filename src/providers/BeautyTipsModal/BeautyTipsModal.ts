import { BEAUTY_TIPS } from '../SalonAppUser-Interface';
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from '../global/global';
import { BehaviorSubject } from "rxjs";
import { Storage } from "@ionic/storage";


@Injectable()
export class BeautyTipsModal {

  /* Global Vairables */
  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;


  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider, ) {
    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('database is setup');
          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }

  getDatabaseState() {
    return this.databaseReady.asObservable()
  }

  createBeautTipsTable() {
    console.log('came in beauty_tip_categories Modal');

    let createTableQuery = "CREATE TABLE IF NOT EXISTS  `beauty_tip_categories` (`btc_id` int(11) NOT NULL,`btc_name` varchar(200) NOT NULL,`btc_image` varchar(250) NOT NULL,`btc_status` tinyint(6), `btc_created_date` timestamp  DEFAULT CURRENT_TIMESTAMP ,`btc_updated_date` timestamp DEFAULT CURRENT_TIMESTAMP) "
    let isTableCreated = false
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        isTableCreated = true
        console.log('beauty_tip_categories table created');
        
        return res
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }


        console.log('error: InsertInToBeautyTipsTable ' + e.message);
        let errorMessage: string = e.message
        if (errorMessage.trim().toLowerCase() === 'table beauty_tip_categories already exists'.trim().toLowerCase()) {
          isTableCreated = true
          return isTableCreated
        } else {
          isTableCreated = false
          return isTableCreated
        }
      });
  }

  InsertInToBeautyTipsTable(beautyTipsData: BEAUTY_TIPS[]) {
    if (!beautyTipsData || beautyTipsData.length === 0) {
      return
    }


    let firstPart = "INSERT INTO `beauty_tip_categories` (`btc_id`, `btc_name`, `btc_image`, `btc_status`, `btc_created_date`, `btc_updated_date`) VALUES ";
    let secondPart = ""
    console.log('beauty_tip_categories', beautyTipsData);
    let beautyTipsIds = ""

    console.log('insertionTest: ' + beautyTipsData.length + ' beauty_tip_categories were sent for dumping');

    beautyTipsData.forEach(beautyTip => {
      secondPart += '("' + beautyTip.btc_id + '", "' + beautyTip.btc_name + '", "' + beautyTip.btc_image + '", "' + beautyTip.btc_status + '" , "' + beautyTip.btc_created_date + '", "' + beautyTip.btc_updated_date + '"),';
      beautyTipsIds += beautyTip.btc_id +','
    });

    let finalQuery = firstPart + secondPart
    let slicedQuery = finalQuery.slice(0, finalQuery.length - 1)
    beautyTipsIds = beautyTipsIds.slice(0, beautyTipsIds.length - 1)
    console.log(slicedQuery);


    return this.delteDuplicateBeautyTips(beautyTipsIds).then(() => {
      let isDataInserted = false
      return this.database.executeSql(slicedQuery, [])
        .then(res => {
          console.log('insertionTest: ' + res.rowsAffected + ' beauty_tip_categories were dumped');
          res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
          return isDataInserted
        })
        .catch(e => {
          this.serviceManager.sendErrorToServer(e.message, slicedQuery)
          isDataInserted = false
          return e.message
        });

    }).catch(e => {
      this.serviceManager.sendErrorToServer(e.message, slicedQuery)
      return e.message
    });
  }

  getBeautyTips(btc_id) {
    
  //  let Inserquery = "select * from `beauty_tip_categories` "
  let Inserquery =null;
  if(btc_id){
    
    Inserquery = 'select * from `beauty_tip_categories` where btc_id NOT IN ('+btc_id+')'
    
  }else{
   
    Inserquery = 'select * from `beauty_tip_categories` '
  }
    
    let beautyTips = []
    return this.database.executeSql(Inserquery, [])
      .then(beautyTipsDataSource => {
        for (var i = 0; i < beautyTipsDataSource.rows.length; i++) {
          beautyTips.push(beautyTipsDataSource.rows.item(i))
        }
        return beautyTips
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, Inserquery)
        console.log('error:  beautyTipsDataSource ' + e.message);

        return beautyTips

      });
  }

  TruncateBeautyTipsTable() {
    let deleteQuery = "DELETE FROM `beauty_tip_categories` ;"
    let isDeleted = false

    return this.database.executeSql(deleteQuery, [])
      .then(res => {
        

        isDeleted = true
        return isDeleted
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteQuery)
        console.log('error: beauty_tip_categories');

        isDeleted = false
        return e.message

      });
  }

  delteDuplicateBeautyTips(beautyTipsIds) {

    if (!beautyTipsIds) {
      return
    }
    let deleteSelectedQuery = 'DELETE FROM `beauty_tip_categories`  where btc_id IN (' + beautyTipsIds + ');'
    return this.database.executeSql(deleteSelectedQuery, [])
      .then(res => {
        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery)
        return false;
      });
  }
}
