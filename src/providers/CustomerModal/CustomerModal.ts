import { Customer } from './../SalonAppUser-Interface';
import { Platform } from 'ionic-angular';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { GlobalProvider } from './../../providers/global/global';
import { BehaviorSubject } from "rxjs/Rx";
import { Storage } from "@ionic/storage";



@Injectable()
export class CustomerModal {

  /* Global Vairables */
  database: SQLiteObject
  private databaseReady: BehaviorSubject<boolean>;


  constructor(
    public http: HttpClient,
    private sqlite: SQLite,
    public sQLitePorter: SQLitePorter,
    public storage: Storage,
    public platform: Platform,
    public serviceManager: GlobalProvider, ) {
    console.log('Hello Customer Modal ');

    this.databaseReady = new BehaviorSubject(false)
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.database = db;
          console.log('database is setup');
          this.databaseReady.next(true)

        }, err => {
          console.log('database is nullll');

        })
    });
  }

  getDatabaseState() {
    return this.databaseReady.asObservable()
  }

  createCustomerTable() {

    let createTableQuery = "CREATE TABLE IF NOT EXISTS  customers (`cust_address` varchar(1000) NOT NULL, `cust_city` varchar(500) NOT NULL, `cust_province` varchar(500) NOT NULL,  `cust_id` int(11) NOT NULL,`cust_name` varchar(64) NOT NULL,`cust_pic` varchar(255) NOT NULL,`cust_zip` varchar(16) NOT NULL,`cust_phone` varchar(16) NOT NULL,`cust_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`cust_device_id` varchar(512) NOT NULL,`cust_lat` varchar(32) NOT NULL,`cust_lng` varchar(32) NOT NULL,`cust_device_type` tinyint(1) NOT NULL DEFAULT '1',`cust_credits` tinyint(6) NOT NULL DEFAULT '100',`referrer_code` varchar(16) NOT NULL,`auth_code_expiry` datetime NOT NULL,`sal_id` int(6) NOT NULL,`cust_email` varchar(120) NOT NULL,`cust_gender` int(1) DEFAULT NULL, `cust_store_credit` int(6));"
    let isTableCreated = false
    return this.database.executeSql(createTableQuery, [])
      .then(res => {
        isTableCreated = true
        console.log('customers table created');
        return isTableCreated
      })
      .catch(e => {
        let msg: string = e.message
        if (!msg.includes('already exists')) {
          this.serviceManager.sendErrorToServer(e.message, createTableQuery)
        }
        console.log('error: customers' + e.message);
        let errorMessage: string = e.message
        if (errorMessage.trim().toLowerCase() === 'table customers already exists'.trim().toLowerCase()) {
          isTableCreated = true
          return isTableCreated
        } else {
          isTableCreated = false
          return isTableCreated
        }
      });
  }
  InsertInToCustomerTable(myCustomer: Customer) {

    if (!myCustomer) return
    let customer: Customer = myCustomer

    let basePart = "INSERT OR REPLACE INTO `customers` (`cust_address`, `cust_city`, `cust_province`, `cust_id`, `cust_name`, `cust_pic`, `cust_zip`, `cust_phone`, `cust_datetime`, `cust_device_id`, `cust_lat`, `cust_lng`, `cust_device_type`, `cust_credits`, `referrer_code`, `auth_code_expiry`, `sal_id`, `cust_email`, `cust_gender`,`cust_store_credit`) VALUES"

    let valuesPart = '("'
      + customer.cust_address + '", "'
      + customer.cust_city + '", "'
      + customer.cust_province + '", "'
      + customer.cust_id + '", "'
      + customer.cust_name + '", "'
      + customer.cust_pic + '", "'
      + customer.cust_zip + '" , "'
      + customer.cust_phone + '", "'
      + customer.cust_datetime + '", "'
      + customer.cust_device_id + '", "'
      + customer.cust_lat + '", "'
      + customer.cust_lng + '", "'
      + customer.cust_device_type + '", "'
      + customer.cust_credits + '", "'
      + customer.referrer_code + '", "'
      + customer.auth_code_expiry + '", "'
      + customer.sal_id + '", "'
      + customer.cust_email + '", "'
      + customer.cust_gender + '", "'
      + customer.cust_store_credit + '")';

    let finalQuery = basePart + valuesPart

    return this.TruncCateCustomerTable().then(isDeleted => {
      let isDataInserted = false
      return this.database.executeSql(finalQuery, []).then(res => {
        console.log('customer insert');
        console.log(res);


        res.rowsAffected && res.rowsAffected > 0 ? isDataInserted = true : isDataInserted = false
        return isDataInserted
      }).catch(e => {
        this.serviceManager.sendErrorToServer(e.message, finalQuery)
        isDataInserted = false
        return e.message

      });
    })


    // return this.TruncCateCustomerTable().then(tblHomeDeleted => {
    //   let isDataInserted = false


    // })
  }

  getCustomer() {
    let selectQuery = "SELECT * FROM `customers`;"
    let myCustomer: Customer
    return this.database.executeSql(selectQuery, [])
      .then(cust => {

        if (cust.rows.length > 0) {
          myCustomer = cust.rows.item(0)
        }
        return myCustomer
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, selectQuery)
        console.log('error: while fetching customer');

        return myCustomer

      });
  }

  //  
  TruncCateCustomerTable() {
    let deleteQuery = "DELETE FROM `customers`; "
    let isDeleted = false
    return this.database.executeSql(deleteQuery, [])
      .then(res => {
        console.log('success: app_home_page_section deleted');
        isDeleted = true
        console.log('deleted');

        return isDeleted
      })
      .catch(e => {
        isDeleted = false
        this.serviceManager.sendErrorToServer(e.message, deleteQuery)
        console.log('error: test deletion ' + e.message);

        return e.message

      });
  }
  dropTable() {

    let deleteSelectedQuery = 'DELETE FROM search;'
    return this.database.executeSql(deleteSelectedQuery, [])
      .then(res => {
        return true;
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, deleteSelectedQuery)
        return false;
      });
  }
  updateCustomerTable(lat, lng) {
    let updateQuery = "UPDATE `customers` SET `cust_lat` =  " + lat + ", `cust_lng` = " + lng
    return this.database.executeSql(updateQuery, [])
      .then(res => {
        console.log('success: customer updated');
        return true
      })
      .catch(e => {
        this.serviceManager.sendErrorToServer(e.message, updateQuery)
        console.log('error: ' + e.message);
        return false
      });
  }
  async updateCustomerStoreCredit(cust_creditslat) {
    let updateQuery = "UPDATE `customers` SET `cust_store_credit` =  " + cust_creditslat
    try {
      const res = await this.database.executeSql(updateQuery, []);
      console.log('success: customer updated');
      return true;
    }
    catch (e) {

      this.serviceManager.sendErrorToServer(e.message, updateQuery);
      console.log('error: ' + e.message);
      return false;
    }
  }
}
