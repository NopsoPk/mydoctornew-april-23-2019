import { Component, Input, ElementRef, Renderer, Renderer2 } from '@angular/core';
import { Events } from 'ionic-angular';

import { Platform } from 'ionic-angular';


@Component({
  selector: 'expandable-header',
  templateUrl: 'expandable-header.html'
})
export class ExpandableHeader {

  @Input('scrollArea') scrollArea: any;
  @Input('headerHeight') headerHeight: number;

  headerScrolHeight=150;
  isAndroidPlatForm=true;
  platForm:any

  newHeaderHeight: any;
  heightIsStillZero = false
  isEventPublished = false
  isFalseEVentPublished = false
  constructor(
    public plt: Platform,
    public element: ElementRef,
    public renderer: Renderer,
    public renderer2: Renderer2,
    public events: Events,) {

    if (this.plt.is('ios')) {
      this.headerScrolHeight=130;
      this.isAndroidPlatForm=false;
       this.platForm="ios"
    }else{
      this.headerScrolHeight=150;
      this.isAndroidPlatForm=false;
      this.platForm="android"
    }


  }

  ngOnInit() {

    this.renderer.setElementStyle(this.element.nativeElement, 'height', this.headerHeight + 'px');

    this.scrollArea.ionScroll.subscribe((ev) => {
      this.resizeHeader(ev);
    });

  }

  resizeHeader(ev) {

    ev.domWrite(() => {
      // console.log(JSON.stringify(ev));

      this.newHeaderHeight = this.headerHeight - ev.scrollTop;


      if (this.newHeaderHeight < 0) {
        this.newHeaderHeight = 0;
        this.heightIsStillZero = true
        this.isFalseEVentPublished = false
      } else {
        this.heightIsStillZero = false
        this.isEventPublished = false
        if (!this.isFalseEVentPublished) {
          this.events.publish('HeaderIsHidden', false);
          this.isFalseEVentPublished = true
        }

      }


      if (this.heightIsStillZero === true && this.isEventPublished === false) {

        this.events.publish('HeaderIsHidden', true);
        this.isEventPublished = true
      }

      
      console.log('this.headerHeight - ev.scrollTop = this.newHeaderHeight true');
      console.log(this.headerHeight + ' - ' + ev.scrollTop + ' = ' + this.newHeaderHeight);

      this.renderer.setElementStyle(this.element.nativeElement, 'height', this.newHeaderHeight + 'px');

      for (let headerElement of this.element.nativeElement.children) {

        let totalHeight = headerElement.offsetTop + headerElement.clientHeight;
        // console.log(JSON.stringify(headerElement));

        if (totalHeight > this.newHeaderHeight+50 && !headerElement.isHidden && this.newHeaderHeight+50 <= this.headerScrolHeight) {

          headerElement.isHidden = true;
          this.renderer.setElementStyle(headerElement, 'opacity', '0');
          this.renderer2.setStyle(headerElement, 'visibility', 'hidden');
          // this.renderer.setElementStyle(headerElement, 'display', 'none');

          // console.log('this.headerHeight - ev.scrollTop = this.newHeaderHeight true');
          // console.log(this.headerHeight + ' - ' + ev.scrollTop + ' = ' + this.newHeaderHeight);

        } else if (totalHeight <= this.newHeaderHeight+50 && headerElement.isHidden) {
          headerElement.isHidden = false;
          this.renderer.setElementStyle(headerElement, 'opacity', '1');
          this.renderer2.setStyle(headerElement, 'visibility', 'visible');
          // this.renderer.setElementStyle(headerElement, 'display', 'block');
          // console.log('this.headerHeight - ev.scrollTop = this.newHeaderHeight false');
          // console.log(this.headerHeight + ' - ' + ev.scrollTop + ' = ' + this.newHeaderHeight);
          
        }

      }

    });

  }

}