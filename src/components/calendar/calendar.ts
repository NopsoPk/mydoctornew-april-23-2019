import { Component, OnInit, Input, Output, EventEmitter,ViewChild } from '@angular/core';
import { Device } from '@ionic-native/device';
import { retry } from 'rxjs/operator/retry';
import {Platform} from 'ionic-angular';
import $ from "jquery";
import { _def } from '../../../node_modules/@angular/core/src/view/provider';
import { OFFERS } from '../../providers/SalonAppUser-Interface';



@Component({
  selector: 'calendar',
  templateUrl: 'calendar.html'
})
export class CalendarComponent implements OnInit {

  @Output() dateSelected = new EventEmitter<Date>()
  @Input() calendarOptions = [];

  @ViewChild('myScroll') myScroll: any;

  public  promotionsdays = []
  public alertShown = false;
  public selectedIndex  = 0;
  public datesArray = [];
  public dateObjsArray = [];
  public months = ["January","February","March","April","May","June",
                  "July","August","September","October","November","December"];
  public days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  
  public dateAndMonths = ''
  public MonthName = ''
  public YearName = ''

  public offersObjects: OFFERS[] = []
  public PromotionsDaysString = ''

  constructor(private device: Device,public platform: Platform) {
    
      
  }

  ngOnInit() {

    
    console.log('Options: ', JSON.stringify(this.calendarOptions));
    this.offersObjects = this.calendarOptions['offersObjects']
    if (this.offersObjects) {
      this.offersObjects.forEach(objOffer => {
        this.PromotionsDaysString += objOffer.so_days + ','+objOffer.so_days
      });
    }else{
      console.log('no promotions avialble');
      
    }
    
    let _promotiondaysArray = this.PromotionsDaysString.split(',')
    
    _promotiondaysArray.forEach(pday => {
      let isValidPromoDay = Number(pday)
      
      if (isValidPromoDay && this.promotionsdays.indexOf(isValidPromoDay) === -1) {
        this.promotionsdays.push(isValidPromoDay)  
      }
    });
    
    for (let i = 0; i < this.calendarOptions['numberOfDays']; i++){
      let date = new Date().getTime() + (i * 86400000);

      if (this.calendarOptions['tomorrow'] != undefined){
        // date = date + 86400000;
        date = date + 3600000;
      }
      
     
        let myDate = new Date(date);
      var _dayNumber = myDate.getDay()
      var gotPromotionForTheDay
      // console.log('day Number ' + _dayNumber);
      if (_dayNumber === 0) {
        _dayNumber = 7
      }
      this.promotionsdays.every(function (promoDayNumber, index) {
        // console.log('promoDayNumber === _dayNumber '  +promoDayNumber + ' ==== '+ _dayNumber + ' '+ (promoDayNumber === _dayNumber));
        
        if(promoDayNumber === _dayNumber){
          gotPromotionForTheDay = _dayNumber
          return false
        }else return true
      })
      
      // console.log('gotPromotionForTheDay '+gotPromotionForTheDay);
      
        var dateObj = {
          date:new Date(date).getDate(),
          day: this.days[new Date(date).getDay()],
          dayNumber: gotPromotionForTheDay,
          monthAndYear: this.months[new Date(date).getMonth()] +" "+ new Date(date).getFullYear(),
        }
      
      // console.log(JSON.stringify(dateObj));
      
      this.datesArray.push(dateObj);
      gotPromotionForTheDay = null


      // alert(this.calendarOptions['appoDate']);
      if (this.calendarOptions['appoDate'] != 'N/A' && this.calendarOptions['appoDate'] != undefined){
        if (this.calendarOptions['appoDate'].getDate() == new Date(date).getDate() && this.calendarOptions['appoDate'].getMonth() == new Date(date).getMonth()){
            this.selectedIndex = i;
        } else {
          
          console.log('not it....' + new Date(date) + '====' + this.calendarOptions['appoDate']);
        }
      }

      this.dateObjsArray.push(new Date(date));
      
    }

    // this.dateAndMonths = this.datesArray[0].monthAndYear
    // let TempDateAndMonths = this.dateAndMonths.split(' ')
    
    // this.MonthName = TempDateAndMonths[0]
    // this.YearName = TempDateAndMonths[1]
    this.getMonthAndYear(0)
  }

  selectDate(date){

  }

  dateClicked(index) {
    // alert('dateClicked '+index)
    this.selectedIndex = index;
    this.dateSelected.emit(this.dateObjsArray[index]);
    console.log('this object is mine');
    
    console.log(JSON.stringify(this.dateObjsArray[index]));
    this.getMonthAndYear(index)
    
  }

  getMonthAndYear(index) {
    if(!this.datesArray || this.datesArray.length === 0){
      return
    }
    this.dateAndMonths = this.datesArray[index].monthAndYear//here it's showing null
    let TempDateAndMonths = this.dateAndMonths.split(' ')
    
    this.MonthName = TempDateAndMonths[0]
    this.YearName = TempDateAndMonths[1]
  }
  getMarginLeft(){
    let totalContentWidth = 87 + (25 * 6);
    let difference = this.platform.width() - totalContentWidth;
    return difference / 12 + 'pt'
  }

  public ScrollLeft(): void {
    // this.myScroll.scrollElement.scrollTop -= 10;
    // if (this.myScroll.scrollElement.scrollLeft < 0) {
    //   this.myScroll.scrollElement.scrollLeft = 0;
    // }
  }

  public scrollToRight(): void {
    // this.myScroll.scrollElement.scrollTop += 10;
    
  }
  
//   $("button").click(function() {
//     $('html,body').animate({
//         scrollTop: $(".second").offset().top},
//         'slow');
// });
  
  
 
}
